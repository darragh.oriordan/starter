import sequelize from "sequelize";
import { sessionFactory } from "../features/sessions/datamodels/session";
import { userFactory } from "../features/users/datamodels/user";
import { IDb } from "./IDb";

const env = process.env.NODE_ENV || "development";

export const db: IDb = ((): IDb => {
  const envConfig = require(`${__dirname}/config/config`)[env];

  const sequelizeInstance: sequelize.Sequelize = createSequelizeInstance(
    envConfig
  );

  const databaseInstance: IDb = {
    Sequelize: sequelizeInstance.Sequelize,
    Session: sessionFactory(sequelizeInstance),
    User: userFactory(sequelizeInstance),
    sequelize: sequelizeInstance
  };

  return databaseInstance;
})();

function createSequelizeInstance(config: any): sequelize.Sequelize {
  if (config.use_env_variable) {
    const databaseUrlString = process.env[config.use_env_variable];
    if (databaseUrlString === undefined) {
      throw new Error(
        "Cannot initialise the database. " +
          "The database url string is not configured correctly" +
          "in the environment variables."
      );
    }
    return new sequelize(databaseUrlString, config);
  } else {
    return new sequelize(
      config.database,
      config.username,
      config.password,
      config
    );
  }
}
