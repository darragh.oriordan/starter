"use strict";

module.exports = {
    down: (queryInterface, Sequelize) => {
        const productionEnvironment = "production";
        if (process.env.NODE_ENV === productionEnvironment) {
            throw new DestructiveToEnvironmentError(productionEnvironment);
        }
        return queryInterface.dropTable("BruteStore");
    },
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            "BruteStore",
            {
                count: Sequelize.INTEGER,
                createdAt: {
                    allowNull: false,
                    type: Sequelize.DATE
                },
                firstRequest: Sequelize.DATE,

                key: {
                    primaryKey: true,
                    type: Sequelize.STRING
                },
                lifetime: Sequelize.DATE,

                lastRequest: Sequelize.DATE,

                updatedAt: {
                    allowNull: false,
                    type: Sequelize.DATE
                }
            },
            { freezeTableName: true }
        );
    }
};
