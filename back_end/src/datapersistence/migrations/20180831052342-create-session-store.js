"use strict";
module.exports = {
    down: (queryInterface, Sequelize) => {
        const productionEnvironment = "production";
        if (process.env.NODE_ENV === productionEnvironment) {
            throw new DestructiveToEnvironmentError(productionEnvironment);
        }
        return queryInterface.dropTable("Sessions");
    },
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable("Sessions", {
            data: { type: Sequelize.TEXT },
            expires: { type: Sequelize.DATE },
            sid: {
                primaryKey: true,
                type: Sequelize.STRING(32)
            }
        });
    }
};
