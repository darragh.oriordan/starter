"use strict";
module.exports = {
    down: (queryInterface, Sequelize) => {
        const productionEnvironment = "production";
        if (process.env.NODE_ENV === productionEnvironment) {
            throw new DestructiveToEnvironmentError(productionEnvironment);
        }
        return queryInterface.dropTable("Users");
    },
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable("Users", {
            activationKey: {
                allowNull: true,
                type: Sequelize.STRING
            },
            city: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            deletedAt: Sequelize.DATE,
            email: {
                allowNull: false,
                type: Sequelize.STRING,
                unique: true
            },
            firstname: {
                allowNull: false,
                type: Sequelize.STRING
            },
            googleAccountId: {
                type: Sequelize.STRING
            },
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            lastname: {
                allowNull: false,
                type: Sequelize.STRING
            },
            password: {
                type: Sequelize.TEXT
            },
            passwordSalt: {
                type: Sequelize.STRING
            },
            phone: {
                type: Sequelize.STRING
            },
            postCode: {
                type: Sequelize.STRING
            },
            resetPasswordKey: {
                allowNull: true,
                type: Sequelize.STRING
            },
            roles: {
                allowNull: true,
                type: Sequelize.STRING
            },
            streetAddress: {
                type: Sequelize.STRING
            },
            suburb: {
                type: Sequelize.STRING
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            username: {
                allowNull: false,
                type: Sequelize.STRING,
                unique: true
            },
            verified: {
                allowNull: true,
                type: Sequelize.BOOLEAN
            },
            version: Sequelize.INTEGER
        });
    }
};
