/*
This file stays as js for now so that the sequelize tooling
can pick it up. We could point that tooling at the typescript output folder
later but it doesn't change much.
*/

var dotenv = require("dotenv");

dotenv.config();

module.exports = {
    development: {
        dialect: "postgres",
        operatorsAliases: false,
        pool: {
            idle: 10000,
            max: 20,
            min: 0
        },
        use_env_variable: "DATABASE_URL"
    },
    production: {
        dialect: "postgres",
        operatorsAliases: false,
        pool: {
            idle: 10000,
            max: 20,
            min: 0
        },
        use_env_variable: "DATABASE_URL"
    },
    test: {
        dialect: "postgres",
        operatorsAliases: false,
        pool: {
            idle: 10000,
            max: 20,
            min: 0
        },
        use_env_variable: "DATABASE_URL"
    }
};
