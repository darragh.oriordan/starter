import sequelize from "sequelize";

import {
    ISessionAttributes,
    ISessionInstance
} from "../features/sessions/datamodels/session";
import { IUserModel } from "../features/users/datamodels/user";

export interface IDb {
    sequelize: sequelize.Sequelize;
    Sequelize: sequelize.SequelizeStatic;
    User: IUserModel;
    Session: sequelize.Model<ISessionInstance, ISessionAttributes>;
}
