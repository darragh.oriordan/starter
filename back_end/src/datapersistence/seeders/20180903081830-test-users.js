"use strict";

// uses js and exports to be compatible with the sequelize commandline tooling
// without going through tsc first.

module.exports = {
    down: (queryInterface, Sequelize) => {
        const productionEnvironment = "production";
        if (process.env.NODE_ENV === productionEnvironment) {
            throw new DestructiveToEnvironmentError(productionEnvironment);
        }
        return queryInterface.bulkDelete("Users", null, {});
    },
    up: (queryInterface, Sequelize) => {
        const productionEnvironment = "production";
        if (process.env.NODE_ENV === productionEnvironment) {
            throw new DestructiveToEnvironmentError(productionEnvironment);
        }
        return queryInterface.bulkInsert(
            "Users",
            [
                {
                    city: "Auckland",
                    createdAt: new Date(),
                    email: 'user1@gmail.com"',
                    firstname: "user1first",
                    lastname: "user1last",
                    // plaintext "user1"
                    password:
                        "90fd31e1e1ec18b7e9b022cebfbb73094a73b4a11e6e43bb1fb94d102b2412e127efc561964a1423a634cffbd2c38e2b02a5ce19c434dc1760189aae64888ef1201740857edb9d87f39fd96803cd60c03d069b5d2989fd23ae17332207d3e19a8e604a8c195b38cba6af55df1b18fa9078e0aa8ba9e54c494a0ad91ba6d0fd56337c571187338bf9af6cf2b6f5a257c25dedcba64fef2f7df45026ccecb2b40ff58ef2d788037dcb9a4ad7cbebfd0019f4082d18a42ba49ae8a0451eee2edd0e3f1eae27bcfac238db73422a2d0695930c46935d2e7795d5cea9ab7b656c693394efdbc6eea6b8a7361da10c98400169d8d495cd2f26918cac9daa19b3689933e70d0acfbb03bd23b7aa6d7abcec3303e23a06888c0b59f829c54eec7b2b18b506bd2d12df283de6babba3f770f4a69e97bc78ff35e22dfa751fe472c4214c1f132391024e14d547301a05f9298c4d3908b69671ad3024ea13603c480171799819f3b5f5cfefaa74e8d1878c7d318af942c3647b04539c79298e0df953c2db522f738c6016f5c88a21adfd9e499b8e175ff7452e3e06f4693306dee30009bfd4a76e7afa5c0f4c9493cd887a587d50e532c262aa7ec37573f3ed4ab10d08e01811041f0ad7f5e4e9c3cc97f2addd9a0d50369b792a27d1c374a0ecae7a8fbffc29831118b340a2f05f9d919cddc79c5cf7efdc70368c9a12fac6c8fd6a8b82ba",
                    passwordSalt:
                        "f2873e1f2a390fc491818de4820dc282525f7c276d508453567d0f78ad45cf14",
                    phone: "02102430256",
                    postCode: "1025",
                    streetAddress: "1024 New North Rd",
                    suburb: "Mt Albert",
                    updatedAt: new Date(),
                    username: "user1@gmail.com"
                },
                {
                    city: "Auckland",
                    createdAt: new Date(),
                    email: "user2@gmail.com",
                    firstname: "user2first",
                    lastname: "user2last",
                    // plaintext "user2"
                    password:
                        "0c26752e1dbd3602fc9f474a282612b0fe142261d28bc084d19a2b0da36abe9e017b74e420135bf891533852b3789b0d775c01c04d595f7145b559698a756a69c6cb50b37aad4a2bf684ea0e95022ded949766c709a766e8a0d9ceaa9aca62c3fa27ad99d8da23e708e3fcf8a3f8090c1c0ea07081eb07fc016d462f4398f112f5e93a113836c7b227176ca0ac804a1a60c011c890dc55693408a57641f6f3be8589b2d8df44261b712958828032ac04df300f1f5525adc532c95322c893775515e685c0ef5d558a0f0ab5e734270180e1f57a60eda19f6601f1f2bdc78e199c32ad33d4c0b6c5d8e9cf40c9d8c09cc738c80e26e3ad0b94befb5a44e9d4f5c7090f68c45803355bfdba6fd17e1c48c773bffc0122df8cf8b26eab020260414f4ff54e18bd0a08a4bf0f14bfb9d14ce0c89ccd37e7baaacfadd9da53066b1626a3582adb76145581fafb2d6ed92e730cc9b3313013f69b7372f70acf1c5d512abfc7ddae7d358d31e8585a88ca05f2510b28c07b1bc9eb8896e484811eca2a9811d8b7da2fd33ed64f7107ff5b088dbc262e9d42d624e7416d416f53bb4e207c0c9aa8c9524a92fff6641c8271a5ba29fbc90939eeae8b0b1f4f2bbd51be2de2489a124827216271e66162372b593fbbffb8bd3a928adac754cc2fd48eb57b43102e7286192a43b16e95e6bba01d38ff6a36463b78db1de5c3b4f67fc03789e7",
                    passwordSalt:
                        "c0b6cd57419c8608a32dc2f7891fd2bf47ca3c0c3edf02ceaf4fdd181a8b1893",
                    phone: "02102430256",
                    postCode: "1025",
                    streetAddress: "1024 New North Rd",
                    suburb: "Mt Albert",
                    updatedAt: new Date(),
                    username: "user2@gmail.com"
                },
                {
                    city: "Auckland",
                    createdAt: new Date(),
                    email: "admin@gmail.com",
                    firstname: "admin",
                    lastname: "admin",
                    // plaintext "user2"
                    password:
                        "0c26752e1dbd3602fc9f474a282612b0fe142261d28bc084d19a2b0da36abe9e017b74e420135bf891533852b3789b0d775c01c04d595f7145b559698a756a69c6cb50b37aad4a2bf684ea0e95022ded949766c709a766e8a0d9ceaa9aca62c3fa27ad99d8da23e708e3fcf8a3f8090c1c0ea07081eb07fc016d462f4398f112f5e93a113836c7b227176ca0ac804a1a60c011c890dc55693408a57641f6f3be8589b2d8df44261b712958828032ac04df300f1f5525adc532c95322c893775515e685c0ef5d558a0f0ab5e734270180e1f57a60eda19f6601f1f2bdc78e199c32ad33d4c0b6c5d8e9cf40c9d8c09cc738c80e26e3ad0b94befb5a44e9d4f5c7090f68c45803355bfdba6fd17e1c48c773bffc0122df8cf8b26eab020260414f4ff54e18bd0a08a4bf0f14bfb9d14ce0c89ccd37e7baaacfadd9da53066b1626a3582adb76145581fafb2d6ed92e730cc9b3313013f69b7372f70acf1c5d512abfc7ddae7d358d31e8585a88ca05f2510b28c07b1bc9eb8896e484811eca2a9811d8b7da2fd33ed64f7107ff5b088dbc262e9d42d624e7416d416f53bb4e207c0c9aa8c9524a92fff6641c8271a5ba29fbc90939eeae8b0b1f4f2bbd51be2de2489a124827216271e66162372b593fbbffb8bd3a928adac754cc2fd48eb57b43102e7286192a43b16e95e6bba01d38ff6a36463b78db1de5c3b4f67fc03789e7",
                    passwordSalt:
                        "c0b6cd57419c8608a32dc2f7891fd2bf47ca3c0c3edf02ceaf4fdd181a8b1893",
                    phone: "02102430256",
                    postCode: "1025",
                    roles: "administrator",
                    streetAddress: "1024 New North Rd",
                    suburb: "Mt Albert",
                    updatedAt: new Date(),
                    username: "admin@gmail.com"
                }
            ],
            {}
        );
    }
};
