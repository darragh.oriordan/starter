export class DestructiveToEnvironmentError extends Error {
    constructor(environment: string) {
        super(
            `This would cause damage to a protected environment (${environment}). Please run this change manually or temporarily remove this protection to apply change.`
        );
    }
}
