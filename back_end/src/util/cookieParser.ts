import { Response } from "express";
import { Response as SuperAgentResponse } from "superagent";
import starterXsrfProtection from "../middleware/starterXsrfProtection";

const xsrfTokenRegEx = new RegExp(
    starterXsrfProtection.RESPONSE_COOKIE_NAME + "=(.*?);"
);
const NOT_FOUND_VALUE = "NOT FOUND";
function parseXsrfTokenCookie(res: Response | SuperAgentResponse): string {
    const foundItems = xsrfTokenRegEx.exec((res.header as any)[
        "set-cookie"
    ] as string);
    if (foundItems && foundItems.length > 0) {
        return foundItems[1] || NOT_FOUND_VALUE;
    }

    return NOT_FOUND_VALUE;
}

export default { NOT_FOUND_VALUE, parseXsrfTokenCookie };
