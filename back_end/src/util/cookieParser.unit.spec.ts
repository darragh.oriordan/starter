import { expect } from "chai";
import {} from "mocha";
import { Response } from "superagent";
import starterXsrfProtection from "../middleware/starterXsrfProtection";
import cookieParser from "./cookieParser";

describe("when parsing session cookies", () => {
    [
        {
            cookieString: `${
                starterXsrfProtection.RESPONSE_COOKIE_NAME
            }=mycookievalue;`,
            expectedParsedValue: "mycookievalue"
        },
        {
            cookieString: `${
                starterXsrfProtection.RESPONSE_COOKIE_NAME
            }=mycookievalue; Max-Age=3600; Path=/; Expires=Fri, 11 Jan 2019 03:02:52 GMT`,
            expectedParsedValue: "mycookievalue"
        },
        {
            cookieString: `${"badname"}=mycookievalue`,
            expectedParsedValue: cookieParser.NOT_FOUND_VALUE
        },
        {
            cookieString: `${"badname"}=mycookievalue; Max-Age=3600; Path=/; Expires=Fri, 11 Jan 2019 03:02:52 GMT`,
            expectedParsedValue: cookieParser.NOT_FOUND_VALUE
        }
    ].forEach((testData: any) => {
        it(`when the cookie value is ${testData.cookieString} should be ${
            testData.expectedParsedValue
        }`, done => {
            const instance = {
                header: {
                    connection: "close",
                    vary: "Origin, Accept-Encoding",
                    ["access-control-allow-credentials"]: "true",
                    ["set-cookie"]: [
                        "csrf_token=IjVY1EUVyiUtAkjyr8HBWJqb; Path=/",
                        testData.cookieString,
                        "sessionId=s%3Ai7pGnoVRHmWYVbDKHC0BCTLL5viv9Flx.HVraKjkFJzDr0j4cIoqhskZXgx5ZthGvAVDQedqODOE; Path=/; Expires=Fri, 25 Jan 2019 02:02:52 GMT; HttpOnly"
                    ],
                    ["accept-ranges"]: "bytes"
                }
            } as Response;

            const actual = cookieParser.parseXsrfTokenCookie(instance);
            // tslint:disable-next-line: no-unused-expression
            expect(actual).to.equal(testData.expectedParsedValue);

            done();
        });
    });
});
