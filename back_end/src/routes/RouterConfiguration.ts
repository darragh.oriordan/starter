import { Router } from "express";

class RouterConfiguration {
    public router: Router;
    public basePath: string;
    constructor(basePath: string) {
        this.router = Router();
        this.basePath = basePath;
    }
}

export default RouterConfiguration;
