import { Application, NextFunction, Request, Response } from "express";

import health from "../features/healthchecks/routes/index";
import accounts from "../features/users/accounts/accountsRoutes";
import profiles from "../features/users/profiles/profileRoutes";
import RouterConfiguration from "./RouterConfiguration";
const API_BASE_ROUTE = "/api/v1";

// Import new routers and add them here.
const routerConfigurations: RouterConfiguration[] = [
  health,
  accounts,
  profiles
];

// Shouldn't have to change anything below here
// --------------------------------------------
export const applyRouters = (app: Application) => {
  routerConfigurations.forEach(routerConfig => {
    app.use(API_BASE_ROUTE + routerConfig.basePath, routerConfig.router);
  });

  // for any other api base routes, return a friendly not found message
  app.use(
    `${API_BASE_ROUTE}/*`,
    (req: Request, res: Response, next: NextFunction) => {
      return res.status(404).json({ message: "Endpoint not found" });
    }
  );
};

export default { API_BASE_ROUTE, applyRouters };
