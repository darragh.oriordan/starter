import { Request } from "express";
import { IUserInstance } from "../features/users/datamodels/user";

// tslint:disable-next-line: interface-name
export interface PassportAuthenticatedRequest extends Request {
    user?: IUserInstance;
}
