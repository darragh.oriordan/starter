import throng from "throng";
import { Server } from "../express/serverInstance";

const numCPUs: throng.WorkerCount = process.env.NUM_CPUS || 1;

// Node entry point
void (async function main() {
    throng(numCPUs, () => new Server().getServer());
})();
