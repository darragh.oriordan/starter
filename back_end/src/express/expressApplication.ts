import compression from "compression";
import express, { Application, Response } from "express";
import expressStatusMonitor from "express-status-monitor";
import helmet from "helmet";
import morgan from "morgan";
import path from "path";
import authenticationStrategy from "../middleware/starterAuthenticationConfiguration";
import starterBruteForceProtection from "../middleware/starterBruteForceProtection";
import starterLimitContentType from "../middleware/starterLimitContentType";
import starterRequestTimeouts, {
  handleHaltOnTimedout
} from "../middleware/starterRequestTimeouts";
import starterSequelizeErrorHandler from "../middleware/starterSequelizeErrorHandler";
import apiRoutes from "../routes/api";

import serveFavicon from "serve-favicon";
import handleApplicationErrors from "../middleware/handleApplicationErrors";
import starterCorsConfiguration from "../middleware/starterCorsConfiguration";
import starterGraphqlConfiguration from "../middleware/starterGraphqlConfiguration";
import starterXsrfProtection from "../middleware/starterXsrfProtection";

export default class ExpressApplicationConfiguration {
  public async getInstance(): Promise<Application> {
    const app = express();

    const publicAssetPath = "../public";
    const frontEndProductionBuildPath = path.join(
      __dirname,
      process.env.FRONT_END_BUILD || "public"
    );
    const indexPath = path.join(
      __dirname,
      process.env.FRONT_END_BUILD || "public",
      "index.html"
    );
    if (!process.env.CI === true) {
      app.use(morgan("dev"));
    }
    app.use(serveFavicon(path.join(__dirname, publicAssetPath, "favicon.ico")));

    const starterCors = starterCorsConfiguration();
    app.use(starterCors);
    app.options("*", starterCors);

    app.use(starterRequestTimeouts());
    app.use(starterLimitContentType());

    app.use(compression());
    app.use(helmet({ frameguard: { action: "deny" } }));

    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));

    app.use(
      `${apiRoutes.API_BASE_ROUTE}/accounts/login`,
      starterBruteForceProtection().prevent
    );

    app.use(handleHaltOnTimedout());
    app.use(authenticationStrategy.configurePassport());
    app.use(handleHaltOnTimedout());

    const xsrfProtection = starterXsrfProtection.getProtectionMiddleware();
    app.use(xsrfProtection);
    app.use(starterXsrfProtection.setXsrfResponseCookie());

    if (process.env.RUN_STATUS_MONITOR === "true") {
      app.use([
        expressStatusMonitor({
          healthChecks: [
            {
              host: process.env.APP_API_HOST,
              path: `${apiRoutes.API_BASE_ROUTE}/healthcheck/db`,
              port: process.env.PORT,
              protocol: process.env.USE_HTTPS ? "https" : "http"
            }
          ],
          path: `/admin/api/status`,
          title: "Api Status"
        })
      ]);
    }
    app.use(handleHaltOnTimedout());

    // route the api requests
    app.use(apiRoutes.API_BASE_ROUTE, xsrfProtection);
    starterGraphqlConfiguration.applyMiddleware({ app });
    apiRoutes.applyRouters(app);
    app.get("/playground", (req: Express.Request, res: Response) => {
      res.sendFile(path.join(__dirname, "/../graphql/index.html"));
    });
    // The handler below will capture everything else (they will all go to the react app)
    app.use(express.static(frontEndProductionBuildPath));
    // Handle single page app routing, return all requests to React app
    app.get("*", (req: Express.Request, res: Response) => {
      res.sendFile(indexPath);
    });

    app.use(handleHaltOnTimedout());

    // Handle errors with state on request object
    app.use(starterSequelizeErrorHandler());
    app.use(handleHaltOnTimedout());

    // handle error object errors
    app.use(handleApplicationErrors());

    return app;
  }
}
