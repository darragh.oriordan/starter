// tslint:disable-next-line:import-name
import dotenv from "dotenv";
import { Application } from "express";
import * as fs from "fs";
import * as http from "http";
import * as https from "https";
import expressApplication from "./expressApplication";

export class Server {
    public static DEFAULT_PORT: string = "5000";
    public static DEFAULT_HTTPS_PORT: string = "5443";

    public static onListening(serverAddress: string | any) {
        const environment = process.env.NODE_ENV || "development";
        const bind =
            typeof serverAddress === "string"
                ? `pipe ${serverAddress}`
                : `port ${serverAddress.port}`;
        // tslint:disable-next-line: no-console
        console.info(`Listening on ${bind} in ${environment} environment`);
    }

    public static onError(
        error: NodeJS.ErrnoException,
        port: string | number
    ): void {
        if (error.syscall !== "listen") {
            throw error;
        }

        const boundInterface =
            typeof port === "string" ? `Pipe ${port} ` : `Port ${port} `;

        if (error.code === "EACCES") {
            // tslint:disable-next-line: no-console
            console.error(`${boundInterface} requires elevated privileges`);
            process.exit(1);
        }
        if (error.code === "EADDRINUSE") {
            // tslint:disable-next-line: no-console
            console.error(`${boundInterface} is already in use`);
            process.exit(1);
        }
        throw error;
    }

    public static normalizePort(val: string): string | number {
        const port = parseInt(val, 10);

        if (isNaN(port)) {
            // running on a named pipe
            return val;
        }

        if (port >= 0) {
            // port number
            return port;
        }

        throw Error("Couldn't configure the port");
    }
    public async getHttpServer(): Promise<http.Server> {
        dotenv.config();

        const expressInstance = await new expressApplication().getInstance();

        return this.initInsecure(expressInstance);
    }

    public async getServer(): Promise<void> {
        try {
            dotenv.config();

            const expressInstance = await new expressApplication().getInstance();

            const developmentEnvironment =
                (process.env.NODE_ENV || "development") === "development";
            const useHttps = process.env.USE_HTTPS === "true";
            if (developmentEnvironment && useHttps) {
                // we will only run the secure server locally for development.
                // https is handled by reverse proxies and routing on production
                await this.initSecure(expressInstance);
            } else {
                await this.initInsecure(expressInstance);
            }
        } catch (error) {
            // tslint:disable-next-line: no-console
            console.error(error);
        }
    }

    public async initInsecure(
        expressInstance: Application
    ): Promise<http.Server> {
        const port = Server.normalizePort(
            process.env.PORT || Server.DEFAULT_PORT
        );
        expressInstance.set("port", port);

        const httpServer: http.Server = await http
            .createServer(expressInstance)
            .listen(port)
            .on("listening", () => Server.onListening(httpServer.address()))
            .on("error", err => Server.onError(err, port));

        return httpServer;
    }

    public async initSecure(expressInstance: Application) {
        const httpsPort = Server.normalizePort(
            process.env.HTTPS_PORT || Server.DEFAULT_HTTPS_PORT
        );

        const httpsOptions: https.ServerOptions = {
            cert: fs.readFileSync("../security/cert.pem"),
            key: fs.readFileSync("../security/cert.key")
        };

        const httpsServer: https.Server = https
            .createServer(httpsOptions, expressInstance)
            .listen(httpsPort)
            .on("listening", () => Server.onListening(httpsServer.address()))
            .on("error", err => Server.onError(err, httpsPort));
    }
}
