export class PaginatedList<T> {
    public totalItems: number;
    public offset: number;
    public limit: number;
    public items: T[];
    public constructor(
        totalItems: number,
        offset: number,
        limit: number,
        items: T[]
    ) {
        this.totalItems = totalItems;
        this.offset = offset;
        this.limit = limit;
        this.items = items;
    }
}
