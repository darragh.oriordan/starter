export class InputValidationError extends Error {
    constructor(message: string) {
        super(message);
    }
}
