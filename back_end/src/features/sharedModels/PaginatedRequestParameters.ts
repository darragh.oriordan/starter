export class PaginatedRequestParameters {
    public offset: number = 0;
    public limit: number = 50;
    public order: string = "";
    public searchString: string = "";
}
