export class StandardRestApiResponse<T> {
    public data: T | undefined;
    public message: string | undefined;
    constructor(responseData?: T, message?: string) {
        this.data = responseData;
        this.message = message;
    }
}
