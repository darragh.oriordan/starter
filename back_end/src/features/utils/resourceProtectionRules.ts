import { IUserInstance } from "../users/datamodels/user";
import rolesUtility, { RoleNames } from "../users/rolesUtility";
import { ResourceAuthorizationError } from "./ResourceAuthorizationError";

function isAuthorisedOwnerOrAdminValidator(
    resourceOwnerId: number,
    user: IUserInstance | undefined
): void {
    if (!user) {
        throw new ResourceAuthorizationError();
    }
    const isAdmin = rolesUtility.isInRole(RoleNames.Administrator, user);

    const isOwner = user.id!.toString() === resourceOwnerId.toString();

    if (!(isAdmin || isOwner)) {
        throw new ResourceAuthorizationError();
    }
}

function isAdministratorValidator(user: IUserInstance | undefined): void {
    if (!user) {
        throw new ResourceAuthorizationError();
    }

    if (!rolesUtility.isInRole(RoleNames.Administrator, user)) {
        throw new ResourceAuthorizationError();
    }
}

export default { isAdministratorValidator, isAuthorisedOwnerOrAdminValidator };
