// tslint:disable-next-line:import-name
import Sequelize from "sequelize";

export interface ISessionAttributes {
  sid: string;
  expires: Date;
  data: string;
}

export interface ISessionInstance
  extends Sequelize.Instance<ISessionAttributes>,
    ISessionAttributes {}

export interface ISessionClass
  extends Sequelize.Model<ISessionInstance, ISessionAttributes> {}

export const sessionFactory = (
  sequelize: Sequelize.Sequelize
): ISessionClass => {
  const attributes: Sequelize.DefineModelAttributes<ISessionAttributes> = {
    data: Sequelize.TEXT,
    expires: Sequelize.DATE,
    sid: {
      primaryKey: true,
      type: Sequelize.STRING(32)
    }
  };

  const sessionModel: ISessionClass = sequelize.define<
    ISessionInstance,
    ISessionAttributes
  >("Session", attributes, {
    timestamps: false
  }) as ISessionClass;

  return sessionModel;
};
