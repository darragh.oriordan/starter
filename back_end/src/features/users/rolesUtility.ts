import { IUserInstance } from "./datamodels/user";

export enum RoleNames {
    Administrator = "administrator"
}

const isInRole = (roleName: RoleNames, user: IUserInstance): boolean => {
    if (!user.roles) {
        return false;
    }
    return user.roles.split(",").includes(roleName);
};

const getRoleAddedString = (
    roleName: RoleNames,
    user: IUserInstance
): string => {
    if (isInRole(roleName, user)) {
        return user.roles;
    }
    let rolesList: string[] = [];
    if (user.roles) {
        rolesList = user.roles.split(",");
    }
    rolesList.push(roleName);

    return rolesList.sort().join(",");
};

const getRoleRemovedString = (
    roleName: RoleNames,
    user: IUserInstance
): string | null => {
    if (!isInRole(roleName, user)) {
        return user.roles === "" ? null : user.roles;
    }

    if (!user.roles) {
        return null;
    }

    const filteredRoleList = user.roles.split(",").filter(x => x !== roleName);

    if (filteredRoleList.length <= 0) {
        return null;
    }

    return filteredRoleList.sort().join(",");
};

export default {
    getRoleAddedString,
    getRoleRemovedString,
    isInRole
};
