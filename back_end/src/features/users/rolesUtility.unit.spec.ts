import { expect } from "chai";
import {} from "mocha";
import { IUserInstance } from "./datamodels/user";
import rolesUtility, { RoleNames } from "./rolesUtility";

describe("when working with user roles", () => {
    [
        {
            existingRoles: null,
            expectedValue: RoleNames.Administrator,
            roleToAdd: RoleNames.Administrator
        },
        {
            existingRoles: "aaaaaaa,bbbbbbbbbbbbbb",
            expectedValue: `aaaaaaa,${RoleNames.Administrator},bbbbbbbbbbbbbb`,
            roleToAdd: RoleNames.Administrator
        },
        {
            existingRoles: "",
            expectedValue: `${RoleNames.Administrator}`,
            roleToAdd: RoleNames.Administrator
        },
        {
            existingRoles: RoleNames.Administrator,
            expectedValue: `${RoleNames.Administrator}`,
            roleToAdd: RoleNames.Administrator
        },
        {
            existingRoles: `aaa,${RoleNames.Administrator},bbb`,
            expectedValue: `aaa,${RoleNames.Administrator},bbb`,
            roleToAdd: RoleNames.Administrator
        }
    ].forEach((testData: any) => {
        it(`when the user has roles "${testData.existingRoles}" and we add ${
            testData.roleToAdd
        }`, done => {
            const instance = {
                roles: testData.existingRoles
            } as IUserInstance;

            const actual = rolesUtility.getRoleAddedString(
                testData.roleToAdd,
                instance
            );
            // tslint:disable-next-line: no-unused-expression
            expect(actual).to.equal(testData.expectedValue);

            done();
        });
    });

    [
        {
            existingRoles: null,
            expectedValue: null,
            roleToRemove: RoleNames.Administrator
        },
        {
            existingRoles: "aaaaaaa,bbbbbbbbbbbbbb",
            expectedValue: `aaaaaaa,bbbbbbbbbbbbbb`,
            roleToRemove: RoleNames.Administrator
        },
        {
            existingRoles: "",
            expectedValue: null,
            roleToRemove: RoleNames.Administrator
        },
        {
            existingRoles: RoleNames.Administrator,
            expectedValue: null,
            roleToRemove: RoleNames.Administrator
        },
        {
            existingRoles: `aaa,${RoleNames.Administrator},bbb`,
            expectedValue: `aaa,bbb`,
            roleToRemove: RoleNames.Administrator
        }
    ].forEach((testData: any) => {
        it(`when the user has roles "${testData.existingRoles}" and we delete ${
            testData.roleToAdd
        }`, done => {
            const instance = {
                roles: testData.existingRoles
            } as IUserInstance;

            const actual = rolesUtility.getRoleRemovedString(
                testData.roleToRemove,
                instance
            );
            // tslint:disable-next-line: no-unused-expression
            expect(actual).to.equal(testData.expectedValue);

            done();
        });
    });
});
