import validator from "validator";

function normalizeEmail(email: string): string {
    return validator.normalizeEmail(validator.escape(email)) || "";
}

function normalizeUsername(username: string): string {
    return username.toLowerCase().trim();
}

export default { normalizeEmail, normalizeUsername };
