import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import chaiHttp from "chai-http";
import http from "http";
import { after, before } from "mocha";

import { Server } from "../../../express/serverInstance";
import starterXsrfProtection from "../../../middleware/starterXsrfProtection";
import cookieParser from "../../../util/cookieParser";
import IProfileInputModel from "./apiModels/IProfileInputModel";

chai.use(chaiAsPromised);
chai.use(chaiHttp);
let server: http.Server;
let agent: ChaiHttp.Agent;
let lastResponsesXsrfKey: string;

describe("Profile controller methods", () => {
  before(async () => {
    server = await new Server().getHttpServer();
    agent = chai.request.agent(server);

    const response = await agent.get("/");
    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(response);
  });

  it("the list of users should be returned for an admin", async () => {
    const loginResponse = await agent
      .post("/api/v1/accounts/login")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send({ password: "user2", username: "admin@gmail.com" });

    expect(loginResponse.status).to.equal(200);
    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(loginResponse);

    const profileListResponse = await agent.get("/api/v1/profiles");

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(
      profileListResponse
    );

    profileListResponse.status.should.equal(200);
    profileListResponse.type.should.equal("application/json");

    expect(profileListResponse.body.data).to.have.length.greaterThan(1);
    profileListResponse.body.data[0].should.include.keys(
      "id",
      "createdAt",
      "updatedAt",
      "city",
      "email",
      "firstname",
      "username",
      "lastname",
      "phone",
      "postCode",
      "streetAddress",
      "suburb",
      "verified"
    );

    const logoutResponse = await agent
      .post("/api/v1/accounts/logout")
      .set("Content-Type", "application/json")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey);
    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(logoutResponse);

    logoutResponse.status.should.equal(200);
  });

  it("can update a profile", async () => {
    const saveableModel = {} as IProfileInputModel;
    const current = new Date().getTime();
    saveableModel.city = "city" + current;
    saveableModel.firstname = "firstname" + current;
    saveableModel.lastname = "lastname" + current;
    saveableModel.phone = "phone" + current;
    saveableModel.postCode = "postCode" + current;
    saveableModel.streetAddress = "streetAddress" + current;
    saveableModel.suburb = "suburb" + current;
    saveableModel.email = "email" + current + "@test.com";
    saveableModel.username = "email" + current + "@test.com";

    const forbiddenUpdateResponse = await agent
      .post("/api/v1/profiles/1")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send(saveableModel);

    forbiddenUpdateResponse.status.should.equal(401);
    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(
      forbiddenUpdateResponse
    );

    const loginResponse = await agent
      .post("/api/v1/accounts/login")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send({ password: "user1", username: "user1@gmail.com" });
    expect(loginResponse.body.data.id).to.equal(1);
    expect(loginResponse.status).to.equal(200);
    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(loginResponse);

    const updateResponse = await agent
      .post("/api/v1/profiles/" + loginResponse.body.data.id)
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send(saveableModel);

    updateResponse.status.should.equal(200);
    updateResponse.type.should.equal("application/json");
    updateResponse.body.data.city.should.equal(saveableModel.city);
    updateResponse.body.data.email.should.equal(saveableModel.email);
    updateResponse.body.data.firstname.should.equal(saveableModel.firstname);
    updateResponse.body.data.phone.should.equal(saveableModel.phone);
    updateResponse.body.data.postCode.should.equal(saveableModel.postCode);
    updateResponse.body.data.streetAddress.should.equal(
      saveableModel.streetAddress
    );
    updateResponse.body.data.suburb.should.equal(saveableModel.suburb);
    updateResponse.body.data.username.should.equal(saveableModel.username);
    updateResponse.body.data.lastname.should.equal(saveableModel.lastname);
  });

  after(async () => {
    agent.close();
  });
});
