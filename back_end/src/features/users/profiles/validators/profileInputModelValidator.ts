import userInputModelValidationRules from "../../userInputModelValidationRules";

import IProfileInputModel from "../apiModels/IProfileInputModel";

const validate = (model: IProfileInputModel): void => {
    userInputModelValidationRules.validateUsername(model.username);
    userInputModelValidationRules.validateEmail(model.email);
};

const isValid = (model: IProfileInputModel): boolean => {
    try {
        validate(model);
        return true;
    } catch {
        return false;
    }
};

export default { validate, isValid };
