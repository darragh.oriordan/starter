import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import chaiHttp from "chai-http";
import http from "http";
import {} from "mocha";
import { Server } from "../../../../express/serverInstance";
import starterXsrfProtection from "../../../../middleware/starterXsrfProtection";
import cookieParser from "../../../../util/cookieParser";

chai.use(chaiAsPromised);
chai.use(chaiHttp);
let server: http.Server;
let agent: ChaiHttp.Agent;
let lastResponsesXsrfKey: string;
const url: string = "/graphql";

describe("users query", () => {
    before(async () => {
        server = await new Server().getHttpServer();
        agent = chai.request.agent(server);

        const getResponse = await agent.get("/");
        lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(getResponse);
    });

    it("admin should be able to query all user profiles", async () => {
        const loginResponse = await agent
            .post("/api/v1/accounts/login")
            .set(
                starterXsrfProtection.REQUEST_HEADER_NAME,
                lastResponsesXsrfKey
            )
            .send({ password: "user2", username: "admin@gmail.com" });

        expect(loginResponse.status).to.equal(200);

        lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(loginResponse);

        const query = {
            query: `
              query {
                    profiles {
                        firstname                
                    }
              }
            `
        };
        // this is not a great test. It Should set up users first so we can guarantee the fields
        // these users are based on initial seeding or other tests.
        const expected = {
            profiles: [
                {
                    firstname: "user1first"
                },
                {
                    firstname: "user2first"
                },
                {
                    firstname: "admin"
                }
            ]
        };
        const profileResponse = await agent
            .post(url)
            .set(
                starterXsrfProtection.REQUEST_HEADER_NAME,
                lastResponsesXsrfKey
            )
            .set("Accept", "application/json")
            .send(query);

        expect(profileResponse.body.data).to.deep.equals(expected);
    });

    after(async () => {
        agent.close();
    });
});
