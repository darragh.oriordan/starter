import { Context } from "apollo-server-core";
import profileService from "../../profiles/profileService";

export const resolvers = {
    Query: {
        me: (parent: any, args: any, ctx: Context) => {
            return ctx.user;
        },
        profiles: async (parent: any, args: any, ctx: Context) => {
            return await profileService.findAll(ctx.user);
        }
    }
};
