export const typeDefs = /* GraphQL */ `
    type User {
        id: ID!
        email:String
        username: String
        firstname: String
        lastname: String
        phone: String
        city: String
        suburb: String
        streetAddress: String
        postCode: String
        createdAt: DateTime!
        updatedAt: DateTime
    }
    extend type Query {
            profiles: [User],
            pagedUsers(rows:Int!, page:Int!,):[User]
            me:User     
    }
`;
