export default interface IProfileModel {
  id: number;
  username: string;
  email: string;
  firstname?: string;
  lastname?: string;
  phone?: string;
  city?: string;
  suburb?: string;
  streetAddress?: string;
  postCode?: string;
  createdAt: Date;
  updatedAt?: Date;
  verified?: boolean;
}
