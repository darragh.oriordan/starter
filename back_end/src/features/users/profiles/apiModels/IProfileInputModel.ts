export default interface IProfileInputModel {
    username: string;
    email: string;
    firstname: string;
    lastname: string;
    phone: string;
    city: string;
    suburb: string;
    streetAddress: string;
    postCode?: string;
}
