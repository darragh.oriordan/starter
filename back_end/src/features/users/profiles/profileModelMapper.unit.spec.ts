import { expect } from "chai";
import {} from "mocha";
import { IUserInstance } from "../datamodels/user";
import profileModelMapper from "./profileModelMapper";

describe("profile model mapper", () => {
    it(`we never return important fields when mapping from user`, done => {
        const instance = {} as IUserInstance;
        instance.email = "test@test";
        instance.get = () => ({ email: "test@test" } as IUserInstance);
        const actual = profileModelMapper.map(instance);
        // tslint:disable-next-line: no-unused-expression
        expect(actual.email).to.exist;
        // tslint:disable-next-line: no-unused-expression
        expect((actual as any).password).to.not.exist;
        // tslint:disable-next-line: no-unused-expression
        expect((actual as any).passwordSalt).to.not.exist;
        // tslint:disable-next-line: no-unused-expression
        expect((actual as any).version).to.not.exist;
        // tslint:disable-next-line: no-unused-expression
        expect((actual as any).activationKey).to.not.exist;
        // tslint:disable-next-line: no-unused-expression
        expect((actual as any).resetPasswordKey).to.not.exist;

        expect(actual.email).to.be.equal(instance.email);
        done();
    });
});
