import { FindOptions, Op, WhereOptions } from "sequelize";
import { PaginatedRequestParameters } from "../../../../features/sharedModels/PaginatedRequestParameters";
import { IUserAttributes } from "../../datamodels/user";

export enum UserOrderBy {
    CreatedAtDesc,
    CreatedAtAsc,
    EmailDesc,
    EmailAsc,
    FirstnameDesc,
    FirstnameAsc,
    LastnameDesc,
    LastnameAsc,
    UsernameDesc,
    UsernameAsc
}
const map = (
    paginationRequest: PaginatedRequestParameters
): FindOptions<IUserAttributes> => {
    if (paginationRequest.limit <= 0) {
        paginationRequest.limit = 50;
    }
    if (paginationRequest.offset < 0) {
        paginationRequest.offset = 0;
    }

    const searchParams: FindOptions<IUserAttributes> = {
        limit: paginationRequest.limit,
        offset: paginationRequest.offset,
        order: getOrder(paginationRequest.order),
        where: getWhereQuery(paginationRequest.searchString)
    };

    return searchParams;
};

function getWhereQuery(
    searchString: string
): undefined | WhereOptions<IUserAttributes> {
    if (!validator.isEmpty(searchString)) {
        const likeSearchString: string = "%" + searchString + "%";

        return {
            [Op.or]: [
                {
                    email: {
                        [Op.like]: likeSearchString
                    }
                },
                {
                    firstname: {
                        [Op.like]: likeSearchString
                    }
                },
                {
                    lastname: {
                        [Op.like]: likeSearchString
                    }
                },
                {
                    username: {
                        [Op.like]: likeSearchString
                    }
                }
            ]
        };
    }

    return undefined;
}

function getOrder(requestedOrder: string): string[] {
    switch (requestedOrder) {
        case UserOrderBy.CreatedAtAsc.toString():
            return ["createdAt", "ASC"];
        case UserOrderBy.CreatedAtDesc.toString():
            return ["createdAt", "DESC"];
        case UserOrderBy.EmailAsc.toString():
            return ["email", "ASC"];
        case UserOrderBy.EmailDesc.toString():
            return ["email", "DESC"];
        case UserOrderBy.FirstnameAsc.toString():
            return ["firstname", "ASC"];
        case UserOrderBy.FirstnameAsc.toString():
            return ["firstname", "DESC"];
        case UserOrderBy.LastnameAsc.toString():
            return ["lastname", "ASC"];
        case UserOrderBy.LastnameAsc.toString():
            return ["lastname", "DESC"];
        case UserOrderBy.UsernameAsc.toString():
            return ["username", "ASC"];
        case UserOrderBy.UsernameAsc.toString():
            return ["username", "DESC"];
        default:
            return ["createdAt", "DESC"];
    }
}
export default { map };
