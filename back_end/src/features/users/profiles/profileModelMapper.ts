import { IUserInstance } from "../datamodels/user";
import IProfileModel from "./apiModels/IProfileModel";

// We don't want to return some user fields for security
// and privacy so we remove them from the response
function map(user: IUserInstance): IProfileModel {
  const profileModel = {} as IProfileModel;
  profileModel.city = user.city;
  profileModel.createdAt = user.createdAt || new Date();
  profileModel.email = user.email;
  profileModel.firstname = user.firstname;
  profileModel.id = user.id || 0;
  profileModel.lastname = user.lastname;
  profileModel.phone = user.phone;
  profileModel.postCode = user.postCode;
  profileModel.streetAddress = user.streetAddress;
  profileModel.suburb = user.suburb;
  profileModel.updatedAt = user.updatedAt;
  profileModel.username = user.username;
  profileModel.verified = user.verified;
  return profileModel;

  //   const returnableUser = user.get({
  //     plain: true
  //   });
  //   delete returnableUser.password;
  //   delete returnableUser.passwordSalt;
  //   delete (returnableUser as any).version;
  //   delete returnableUser.activationKey;
  //   delete returnableUser.resetPasswordKey;
  //   delete returnableUser.googleAccountId;

  //   return returnableUser as IProfileModel;
}

export default { map };
