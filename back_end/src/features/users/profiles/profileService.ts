import { Op } from "sequelize";
import { db } from "../../../datapersistence/DbInstance";
import { PaginatedList } from "../../sharedModels/PaginatedList";
import { PaginatedRequestParameters } from "../../sharedModels/PaginatedRequestParameters";
import { IUserInstance } from "../datamodels/user";
import userPropertyNormalizers from "../userPropertyNormalizers";
import resourceProtectionRules from "./../../../features/utils/resourceProtectionRules";
import IProfileInputModel from "./apiModels/IProfileInputModel";
import IProfileModel from "./apiModels/IProfileModel";
import profileModelMapper from "./profileModelMapper";
import paginationRequestMapper from "./util/paginationRequestMapper";
import profileInputModelValidator from "./validators/profileInputModelValidator";

const findAll = async (
    currentUser: IUserInstance | undefined
): Promise<IProfileModel[]> => {
    resourceProtectionRules.isAdministratorValidator(currentUser);
    const allResults = await db.User.findAll({ order: [["createdAt"]] });

    return allResults.map(profileModelMapper.map);
};

const findAllPaginated = async (
    requestParameters: PaginatedRequestParameters,
    currentUser: IUserInstance | undefined
): Promise<PaginatedList<IProfileModel>> => {
    resourceProtectionRules.isAdministratorValidator(currentUser);
    const databaseQueryParams = paginationRequestMapper.map(requestParameters);

    const allResults = await db.User.findAndCountAll(databaseQueryParams);

    return new PaginatedList<IProfileModel>(
        allResults.count,
        requestParameters.offset,
        requestParameters.limit,
        allResults.rows.map(profileModelMapper.map)
    );
};

const findById = async (
    id: number,
    currentUser: IUserInstance | undefined
): Promise<IProfileModel> => {
    resourceProtectionRules.isAuthorisedOwnerOrAdminValidator(id, currentUser);

    const user = await db.User.findById(id, {
        rejectOnEmpty: true
    });

    return profileModelMapper.map(user!);
};

const findOne = async (
    searchTerm: string,
    currentUser: IUserInstance | undefined
): Promise<IProfileModel> => {
    const user = await db.User.find({
        rejectOnEmpty: true,
        where: {
            [Op.or]: [
                {
                    id: searchTerm
                },
                {
                    email: searchTerm
                },
                {
                    username: searchTerm
                }
            ]
        }
    });
    const nonNullUser = user!;
    resourceProtectionRules.isAuthorisedOwnerOrAdminValidator(
        nonNullUser.id!,
        currentUser
    );
    return profileModelMapper.map(nonNullUser);
};

export const updateOne = async (
    userId: number,
    newProfileData: IProfileInputModel,
    currentUser: IUserInstance | undefined
): Promise<IProfileModel> => {
    resourceProtectionRules.isAuthorisedOwnerOrAdminValidator(
        userId,
        currentUser
    );

    const existingUserModel = await db.User.findOne({
        rejectOnEmpty: true,
        where: { id: userId }
    });

    const notNullExistingUserData = existingUserModel!;
    newProfileData.email = userPropertyNormalizers.normalizeEmail(
        newProfileData.email
    );
    newProfileData.username = userPropertyNormalizers.normalizeEmail(
        newProfileData.username
    );

    profileInputModelValidator.validate(newProfileData);

    const updatedUser = await notNullExistingUserData.update({
        city: newProfileData.city,
        email: newProfileData.email,
        firstname: newProfileData.firstname,
        lastname: newProfileData.lastname,
        phone: newProfileData.phone,
        postCode: newProfileData.postCode,
        streetAddress: newProfileData.streetAddress,
        suburb: newProfileData.suburb,
        username: newProfileData.username
    });
    return profileModelMapper.map(updatedUser);
};

export default {
    findAll,
    findAllPaginated,
    findById,
    findOne,
    updateOne
};
