import { NextFunction, Request, Response } from "express";
import { PassportAuthenticatedRequest } from "PassportAuthenticatedRequest";
import { StandardRestApiResponse } from "../../../features/sharedModels/StandardRestApiResponse";
import IProfileInputModel from "./apiModels/IProfileInputModel";
import profileService from "./profileService";

export const getList = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const users = await profileService.findAll(req.user);

    return res.status(200).json(new StandardRestApiResponse(users));
  } catch (err) {
    return next(err);
  }
};

export const findOne = async (
  req: PassportAuthenticatedRequest,
  res: Response,
  next: NextFunction
) => {
  try {
    const user = await profileService.findOne(req.params.searchTerm, req.user);

    return res.status(200).json(new StandardRestApiResponse(user));
  } catch (err) {
    return next(err);
  }
};

export const findById = async (
  req: PassportAuthenticatedRequest,
  res: Response,
  next: NextFunction
) => {
  try {
    const user = await profileService.findById(req.params.id, req.user);

    return res.status(200).json(new StandardRestApiResponse(user));
  } catch (err) {
    return next(err);
  }
};

export const updateOne = async (
  req: PassportAuthenticatedRequest,
  res: Response,
  next: NextFunction
) => {
  try {
    const updatedUser = await profileService.updateOne(
      req.params.id,
      parseProfileModel(req.body),
      req.user
    );

    return res.status(200).json(new StandardRestApiResponse(updatedUser));
  } catch (err) {
    return next(err);
  }
};

const parseProfileModel = (requestBody: any): IProfileInputModel => {
  const profileModel: IProfileInputModel = {
    city: requestBody.city,
    email: requestBody.email,
    firstname: requestBody.firstname,
    lastname: requestBody.lastname,
    phone: requestBody.phone,
    postCode: requestBody.postCode,
    streetAddress: requestBody.streetAddress,
    suburb: requestBody.suburb,
    username: requestBody.username
  };

  return profileModel;
};
