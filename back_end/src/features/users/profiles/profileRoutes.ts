import starterRouteProtection from "../../../middleware/starterRouteProtection";
import RouterConfiguration from "../../../routes/RouterConfiguration";
import * as profileController from "./profileController";

const routerConfiguration = new RouterConfiguration("/profiles");

/* User Router */
routerConfiguration.router.get("/", [
    starterRouteProtection.isAdministratorRouteProtection,
    profileController.getList
]);
routerConfiguration.router.get("/:id", profileController.findById);
routerConfiguration.router.get(
    "/search/:searchTerm",
    profileController.findOne
);
routerConfiguration.router.post("/:id", profileController.updateOne);

export default routerConfiguration;
