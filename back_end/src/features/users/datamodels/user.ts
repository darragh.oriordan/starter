import passportLocalSequelize = require("passport-local-sequelize");
import * as Sequelize from "sequelize";

export interface IUserAttributes {
  id?: number;
  username: string;
  email: string;
  firstname: string;
  googleAccountId?: string;
  lastname: string;
  password?: string;
  passwordSalt?: string;
  phone?: string;
  city?: string;
  roles: string;
  suburb?: string;
  streetAddress?: string;
  postCode?: string;
  activationKey?: string;
  resetPasswordKey?: string;
  verified?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface IPassportSequelizeMethods {
  createStrategy(): any | undefined;
  // tslint:disable-next-line: ban-types
  setPassword(password: string, callback?: Function): any | undefined;
  // tslint:disable-next-line: ban-types
  setActivationKey(callback?: Function): any | undefined;
  // tslint:disable-next-line: ban-types
  authenticate(password?: string, callback?: Function): any | undefined;
  serializeUser(): any | undefined;
  deserializeUser(): any | undefined;
  // tslint:disable-next-line: ban-types
  register(user: any, password: string, callback?: Function): any | undefined;
  activate(
    username: string,
    password: string,
    activationKey: string,
    // tslint:disable-next-line: ban-types
    cb: Function
  ): any | undefined;
}
class PassportMethodsNotImplementedError extends Error {
  constructor() {
    super("This model has not had passport methods applied");
  }
}
// set up stubs for passportSequelize to prototype
const classMethods: IPassportSequelizeMethods = {
  // tslint:disable-next-line: ban-types
  authenticate: (password?: string, callback?: Function) => {
    throw new PassportMethodsNotImplementedError();
  },
  createStrategy: () => {
    throw new PassportMethodsNotImplementedError();
  },
  deserializeUser: () => {
    throw new PassportMethodsNotImplementedError();
  },
  // tslint:disable-next-line: ban-types
  register: (user: any, password: string, callback?: Function) => {
    throw new PassportMethodsNotImplementedError();
  },
  // tslint:disable-next-line: ban-types
  setActivationKey: (callback?: Function) => {
    throw new PassportMethodsNotImplementedError();
  },
  // tslint:disable-next-line: ban-types
  setPassword: (password: string, callback?: Function) => {
    throw new PassportMethodsNotImplementedError();
  },

  serializeUser: () => {
    /* no-np*/
  },

  activate: (
    username: string,
    password: string,
    activationKey: string,
    // tslint:disable-next-line: ban-types
    cb: Function
  ) => {
    /* no-np*/
  }
};

export interface IUserInstance
  extends Sequelize.Instance<IUserAttributes>,
    IUserAttributes,
    IPassportSequelizeMethods {}

export interface IUserModel
  extends Sequelize.Model<IUserInstance, IUserAttributes>,
    IPassportSequelizeMethods {}

export const userFactory = (sequelize: Sequelize.Sequelize): IUserModel => {
  const attributes: Sequelize.DefineModelAttributes<IUserAttributes> = {
    activationKey: Sequelize.STRING,
    city: {
      type: Sequelize.STRING
    },
    email: {
      allowNull: false,
      type: Sequelize.STRING,
      validate: {
        isEmail: {
          args: true,
          msg: "Please enter valid email address."
        }
      }
    },
    firstname: {
      allowNull: false,
      type: Sequelize.TEXT,
      validate: {
        len: {
          args: [1, 50],
          msg: "Length of first name must be between 1 and 50."
        },
        notEmpty: {
          args: true,
          msg: "First name cannot be empty."
        }
      }
    },
    googleAccountId: Sequelize.STRING,
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    lastname: {
      allowNull: false,
      type: Sequelize.TEXT,
      validate: {
        len: {
          args: [1, 50],
          msg: "Length of last name must be between 1 and 50."
        },
        notEmpty: {
          args: true,
          msg: "Last name cannot be empty."
        }
      }
    },
    password: {
      type: Sequelize.TEXT,
      validate: {
        notEmpty: {
          args: true,
          msg: "Password cannot be empty."
        }
      }
    },
    passwordSalt: {
      type: Sequelize.TEXT,
      validate: {
        notEmpty: {
          args: true,
          msg: "Password salt cannot be empty."
        }
      }
    },
    phone: {
      type: Sequelize.STRING,
      validate: {
        len: {
          args: [4, 25],
          msg: "Please enter valid phone number (length 4-25)"
        }
      }
    },
    postCode: {
      allowNull: true,
      type: Sequelize.STRING
    },
    resetPasswordKey: Sequelize.STRING,
    roles: Sequelize.STRING,
    streetAddress: Sequelize.TEXT,
    suburb: Sequelize.STRING,
    username: {
      allowNull: false,
      type: Sequelize.TEXT,

      validate: {
        len: {
          args: [1, 254],
          msg: "Length of user name must be between 1 and 254."
        },
        notEmpty: {
          args: true,
          msg: "User name cannot be empty space."
        }
      }
    },
    verified: Sequelize.BOOLEAN
  };

  const userModel: IUserModel = sequelize.define<
    IUserInstance,
    IUserAttributes
  >("User", attributes, {
    classMethods,
    indexes: [
      { name: "unique_username", unique: true, fields: ["username"] },
      { name: "unique_email", unique: true, fields: ["email"] }
    ],
    paranoid: true,
    version: true
  }) as IUserModel;

  const attachOptions = {
    activationRequired: true,
    digest: "sha1",
    hashField: "password",
    iterations: 10,
    saltField: "passwordSalt",
    saltlen: 32,
    usernameField: "username",
    usernameLowerCase: true
  };

  passportLocalSequelize.attachToUser(userModel, attachOptions);

  return userModel;
};
