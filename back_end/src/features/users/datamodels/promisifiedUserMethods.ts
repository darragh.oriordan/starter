import { db } from "../../../datapersistence/DbInstance";
import ISignupModel from "../accounts/apimodels/ISignupModel";
import { IUserInstance } from "./user";

function activateAsync(
  username: string,
  password: string,
  activationCode: string
): Promise<IUserInstance> {
  return new Promise((resolve, reject) => {
    db.User.activate(
      username,
      password,
      activationCode,
      (err: any, user: IUserInstance) => {
        if (err) {
          reject(err);
        }
        resolve(user);
      }
    );
  });
}

function authenticateAsync(
  password: string,
  userModel: IUserInstance
): Promise<IUserInstance> {
  return new Promise((resolve, reject) => {
    userModel.authenticate(password, (err: any, user: IUserInstance) => {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
}
function setPasswordAsync(
  password: string,
  userModel: IUserInstance
): Promise<IUserInstance> {
  return new Promise((resolve, reject) => {
    userModel.setPassword(password, (err: any, user: IUserInstance) => {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
}

function registerAsync(userModel: ISignupModel): Promise<IUserInstance> {
  return new Promise<IUserInstance>((resolve, reject) => {
    db.User.register(userModel, userModel.password, (err: any, user: any) => {
      if (err) {
        reject(err);
      }
      resolve(user);
    });
  });
}

export default {
  activateAsync,
  authenticateAsync,
  registerAsync,
  setPasswordAsync
};
