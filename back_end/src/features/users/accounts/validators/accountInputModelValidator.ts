import userInputModelValidationRules from "../../userInputModelValidationRules";
import ISignupModel from "../apimodels/ISignupModel";

const validate = (userModel: ISignupModel): void => {
    userInputModelValidationRules.validatePassword(userModel.password);
    userInputModelValidationRules.validateConfirmPassword(
        userModel.passwordConfirm
    );
    userInputModelValidationRules.validatePasswordMatch(
        userModel.password,
        userModel.passwordConfirm
    );
    userInputModelValidationRules.validateUsername(userModel.username);
    userInputModelValidationRules.validateTandCAcceptance(
        userModel.agreeToTerms
    );
    userInputModelValidationRules.validateEmail(userModel.email);
};

const isValid = (userModel: ISignupModel): boolean => {
    try {
        validate(userModel);
        return true;
    } catch {
        return false;
    }
};

export default { validate, isValid };
