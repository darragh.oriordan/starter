import { expect } from "chai";
import accountsInputModelValidationRules from "../../userInputModelValidationRules";

describe("UserId validator", () => {
    ["1", "2", "12345"].forEach((value, itemNumber, callback) => {
        it(`can parse valid (Value ${value})`, done => {
            expect(() =>
                accountsInputModelValidationRules.validateUserId(value)
            ).not.to.throw();
            done();
        });
    });

    ["0", "-1", "cow", ""].forEach((value, itemNumber, callback) => {
        it(`throws on an invalid (Value ${value})`, done => {
            expect(() =>
                accountsInputModelValidationRules.validateUserId(value)
            ).to.throw(
                accountsInputModelValidationRules.INVALID_USER_ID_MESSAGE
            );
            done();
        });
    });
});

describe("Email validator", () => {
    ["a@abc.com", "a+b@abc.com", "a.b@abc.com"].forEach(
        (value, itemNumber, callback) => {
            it(`can parse a valid (Value ${value})`, done => {
                expect(() =>
                    accountsInputModelValidationRules.validateEmail(value)
                ).not.to.throw();
                done();
            });
        }
    );

    ["@abc.com", "something@", "cow", ""].forEach(
        (value, itemNumber, callback) => {
            it(`throws on an invalid (Value ${value})`, done => {
                expect(() =>
                    accountsInputModelValidationRules.validateEmail(value)
                ).to.throw(
                    accountsInputModelValidationRules.INVALID_EMAIL_MESSAGE
                );
                done();
            });
        }
    );
});

describe("Password validator", () => {
    ["sdfsdf"].forEach((value, itemNumber, callback) => {
        it(`can parse a valid (Value ${value})`, done => {
            expect(() =>
                accountsInputModelValidationRules.validatePassword(value)
            ).not.to.throw();
            done();
        });
    });

    [""].forEach((value, itemNumber, callback) => {
        it(`throws on an invalid (Value ${value})`, done => {
            expect(() =>
                accountsInputModelValidationRules.validatePassword(value)
            ).to.throw(
                accountsInputModelValidationRules.MUST_SUPPLY_PASSWORD_MESSAGE
            );
            done();
        });
    });
});

describe("Password confirm validator", () => {
    it(`can parse a valid`, done => {
        expect(() =>
            accountsInputModelValidationRules.validatePasswordMatch(
                "aaa",
                "aaa"
            )
        ).not.to.throw();
        done();
    });

    it(`throws on an invalid`, done => {
        expect(() =>
            accountsInputModelValidationRules.validatePasswordMatch(
                "aaa",
                "bbb"
            )
        ).to.throw(
            accountsInputModelValidationRules.MUST_MATCH_PASSWORDS_MESSAGE
        );
        done();
    });
});

describe("TandC validator", () => {
    it(`can parse a valid`, done => {
        expect(() =>
            accountsInputModelValidationRules.validateTandCAcceptance(true)
        ).not.to.throw();
        done();
    });

    it(`throws on an invalid`, done => {
        expect(() =>
            accountsInputModelValidationRules.validateTandCAcceptance(false)
        ).to.throw(accountsInputModelValidationRules.INVALID_TANDC_MESSAGE);
        done();
    });
});

describe("Username validator", () => {
    ["sdfsdf"].forEach((value, itemNumber, callback) => {
        it(`can parse a valid (Value ${value})`, done => {
            expect(() =>
                accountsInputModelValidationRules.validateUsername(value)
            ).not.to.throw();
            done();
        });
    });

    [""].forEach((value, itemNumber, callback) => {
        it(`throws on an invalid (Value ${value})`, done => {
            expect(() =>
                accountsInputModelValidationRules.validateUsername(value)
            ).to.throw(
                accountsInputModelValidationRules.INVALID_USERNAME_MESSAGE
            );
            done();
        });
    });
});
