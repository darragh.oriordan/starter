import { IUserInstance } from "../../datamodels/user";

const validate = (userModel: IUserInstance): void => {
  if (!userModel.googleAccountId && !userModel.password) {
    throw new Error("Not enough information to save the account.");
  }
};

const isValid = (userModel: IUserInstance): boolean => {
  try {
    validate(userModel);
    return true;
  } catch {
    return false;
  }
};

export default { validate, isValid };
