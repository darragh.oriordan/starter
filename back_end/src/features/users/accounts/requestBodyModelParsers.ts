import { Request } from "express";
import { IActivateAccountModel } from "./apimodels/IActivateAccountModel";
import { IChangePasswordModel } from "./apimodels/IChangePasswordModel";

function parseActivateAccountModel(request: Request): IActivateAccountModel {
    const model: IActivateAccountModel = {
        activationCode: request.params.activationCode,
        password: request.body.password,
        username: request.body.username
    };

    return model;
}

function parseChangePasswordModel(request: Request): IChangePasswordModel {
    const profileModel: IChangePasswordModel = {
        currentPassword: request.body.currentPassword,
        newPassword: request.body.newPassword,
        newPasswordConfirm: request.body.newPasswordConfirm,
        userId: request.user.id
    };

    return profileModel;
}

export default {
    parseActivateAccountModel,
    parseChangePasswordModel
};
