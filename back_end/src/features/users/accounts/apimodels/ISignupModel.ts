export default interface ISignupModel {
    agreeToTerms: boolean;
    username: string;
    email: string;
    firstname: string;
    lastname: string;
    password: string;
    passwordConfirm: string;
    phone: string;
    city: string;
    suburb: string;
    streetAddress: string;
    postCode?: string;
}
