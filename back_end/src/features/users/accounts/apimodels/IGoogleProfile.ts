export interface IGoogleProfile {
  photos: [];
  displayName: string;
  emails: [{ value: string; type: string }];
  gender: string;
  id: string;
  name: { givenName: string; familyName: string };
}
