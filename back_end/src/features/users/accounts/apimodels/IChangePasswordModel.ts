export interface IChangePasswordModel {
    userId: number;
    currentPassword: string;
    newPassword: string;
    newPasswordConfirm: string;
}
