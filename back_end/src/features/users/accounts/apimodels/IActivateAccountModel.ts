export interface IActivateAccountModel {
    username: string;
    password: string;
    activationCode: string;
}
