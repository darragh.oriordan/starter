import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import chaiHttp from "chai-http";
import http from "http";
import { after, before } from "mocha";

import { Server } from "../../../express/serverInstance";

import { db } from "../../../datapersistence/DbInstance";
import starterXsrfProtection from "../../../middleware/starterXsrfProtection";
import cookieParser from "../../../util/cookieParser";
import { IChangePasswordModel } from "./apimodels/IChangePasswordModel";
import ISignupModel from "./apimodels/ISignupModel";
import authenticationService from "./authenticationService";
import sessionCookieConfiguration from "./sessionCookieConfiguration";

chai.use(chaiAsPromised);
chai.use(chaiHttp);
let server: http.Server;
let agent: ChaiHttp.Agent;
let lastResponsesXsrfKey: string;
let createdUserId: number;
const saveableModel = {
  agreeToTerms: true,
  email: "auser@gmail.com", // normalised email is different to supplied
  firstname: "darragh",
  lastname: "oriordan",
  password: "superpassword",
  passwordConfirm: "superpassword",
  username: "a.user@gmail.com"
} as ISignupModel;

describe("REST signup method tests", () => {
  before(async () => {
    server = await new Server().getHttpServer();
    agent = chai.request.agent(server);

    await agent.get("/").then(res => {
      lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(res);
    });
  });

  it("Missmatched password should fail to signup successfully", async () => {
    const missmatchedPasswordsModel = Object.assign({}, saveableModel);
    missmatchedPasswordsModel.passwordConfirm = "badpasswordconfirm";

    const response = await agent
      .post("/api/v1/accounts/signup")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send(missmatchedPasswordsModel);

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(response);
    chai.expect(response.status).to.equal(400);
  });

  it("signing up successfully should respond with saved user", async () => {
    const response = await agent
      .post("/api/v1/accounts/signup")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send(saveableModel);

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(response);

    expect(response.status).to.equal(200);
    expect(response.type).to.equal("application/json");
    expect(response.body.data).to.include.keys(
      "id",
      "createdAt",
      "updatedAt",
      "email",
      "city",
      "firstname",
      "lastname",
      "phone",
      "postCode",
      "streetAddress",
      "suburb",
      "username",
      "verified"
    );
    expect(response.body.data.email).to.equal(saveableModel.email);
    expect(response.body.data.firstname).to.equal(saveableModel.firstname);
    expect(response.body.data.lastname).to.equal(saveableModel.lastname);
    expect(response.body.data.username).to.equal(saveableModel.username);
    chai.should().exist(response.body.data.createdAt);
    chai.should().not.exist(response.body.data.phone);
    chai.should().not.exist(response.body.data.city);
    chai.should().not.exist(response.body.data.suburb);
    chai.should().not.exist(response.body.data.streetAddress);
    chai.should().not.exist(response.body.data.postCode);
    chai.should().not.exist(response.body.data.verified);
    chai.should().not.exist(response.body.data.deletedAt);

    createdUserId = response.body.data.id;
  });
  it("signing up twice should fail", async () => {
    const response = await agent
      .post("/api/v1/accounts/signup")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send(saveableModel);

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(response);
    expect(response.status).to.equal(400);
    expect(response.type).to.equal("application/json");
    expect(response.body.message).to.equal(
      "User already exists with a.user@gmail.com"
    );
  });

  it("cannot activate with bad key", async () => {
    const badActivationKey = "badkey";

    const response = await agent
      .post("/api/v1/accounts/activate/" + badActivationKey)
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send({
        password: saveableModel.password,
        username: saveableModel.username
      });

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(response);

    expect(response.status).to.equal(400);
    expect(response.type).to.equal("application/json");

    expect(response.body.message).to.equal("Invalid activation key");
  });

  it("can activate with good key", async () => {
    // have to hit db to get good key
    const user = await db.User.findOne({
      rejectOnEmpty: true,
      where: { username: saveableModel.username }
    });

    return agent
      .post("/api/v1/accounts/activate/" + user!.activationKey!)
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .set("Content-Type", "application/json")
      .send({
        password: saveableModel.password,
        username: saveableModel.username
      })
      .then(res => {
        lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(res);

        expect(res.status).to.equal(200);
        expect(res.type).to.equal("application/json");

        expect(res.body.data.username).to.equal(saveableModel.username);
      });
  });
  it("can login", async () => {
    const logoutResponse = await agent
      .post("/api/v1/accounts/logout")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      // because chai can't automatically detect with empty body
      .set("Content-Type", "application/json");

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(logoutResponse);
    const loginResponse = await agent
      .post("/api/v1/accounts/login")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send({
        password: saveableModel.password,
        username: saveableModel.username
      });

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(loginResponse);

    loginResponse.status.should.equal(200);
    loginResponse.type.should.equal("application/json");

    chai.should().exist(loginResponse.body);
    expect(loginResponse).to.have.cookie(
      sessionCookieConfiguration.sessionCookieName
    );
  });

  it("can change password", async () => {
    const newPassword = "newpassword";
    const changePasswordResponse = await agent
      .post("/api/v1/accounts/changepassword")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send({
        currentPassword: saveableModel.password,
        newPassword,
        newPasswordConfirm: newPassword
      } as IChangePasswordModel);
    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(
      changePasswordResponse
    );

    expect(changePasswordResponse.status).to.equal(200);
    expect(changePasswordResponse.type).to.equal("application/json");
    chai.should().exist(changePasswordResponse.body);
    chai.should().exist(changePasswordResponse.body.data.username);

    const logoutResponse = await agent
      .post("/api/v1/accounts/logout")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      // because chai can't automatically detect with empty body
      .set("Content-Type", "application/json");

    expect(logoutResponse.status).to.equal(200);

    // Try logging in again with new password this time.
    const loginResponse = await agent
      .post("/api/v1/accounts/login")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send({
        password: newPassword,
        username: saveableModel.username
      });
    saveableModel.password = newPassword;

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(loginResponse);

    expect(loginResponse.status).to.equal(200);
    expect(loginResponse.type).to.equal("application/json");
    chai.should().exist(loginResponse.body);
    expect(loginResponse).to.have.cookie(
      sessionCookieConfiguration.sessionCookieName
    );
  });

  it("logging out should not have session cookie", async () => {
    const logoutResponse = await agent
      .post("/api/v1/accounts/logout")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      // because chai can't automatically detect with empty body
      .set("Content-Type", "application/json");

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(logoutResponse);

    expect(logoutResponse.status).to.equal(200);
    expect(logoutResponse.type).to.equal("application/json");
    chai.should().exist(logoutResponse.body);
    expect(logoutResponse.body.message).equals(
      authenticationService.LOGGED_OUT_MESSAGE
    );
    expect(logoutResponse).to.not.have.cookie(
      sessionCookieConfiguration.sessionCookieName
    );
  });

  it("logging in to known account with bad credentials will be rejected", async () => {
    const response = await agent
      .post("/api/v1/accounts/login")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send({
        password: "aljsdflkj",
        username: saveableModel.username
      });

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(response);

    expect(response.status).to.equal(400);
    expect(response.type).to.equal("application/json");
    chai.should().exist(response.body);
    chai
      .expect(response)
      .to.not.have.cookie(sessionCookieConfiguration.sessionCookieName);
    response.body.message.should.equal(
      authenticationService.LOGIN_FAILED_PASSWORD_MESSAGE
    );
  });

  it("logging in to unknown account will be rejected", async () => {
    const response = await agent
      .post("/api/v1/accounts/login")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send({
        password: "aljsdflkj",
        username: "sdfsdfsdfsdf"
      });

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(response);

    expect(response.status).to.equal(400);
    expect(response.type).to.equal("application/json");
    chai.should().exist(response.body);
    expect(response).to.not.have.cookie(
      sessionCookieConfiguration.sessionCookieName
    );
    response.body.message.should.equal(
      authenticationService.LOGIN_FAILED_USERNAME_MESSAGE
    );
  });

  it("can delete a known account (when logged in)", async () => {
    const loginResponse = await agent
      .post("/api/v1/accounts/login")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send({
        password: "user2",
        username: "admin@gmail.com"
      });
    expect(loginResponse.status).to.equal(200);
    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(loginResponse);

    const delResponse = await agent
      .del(`/api/v1/accounts/${createdUserId}`)
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey);

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(delResponse);
    expect(delResponse.status).to.equal(200);
    expect(delResponse.body.data.id).to.equal(createdUserId);
    const getResponse = await agent.get(`/api/v1/profiles/${createdUserId}`);

    lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(getResponse);

    expect(getResponse.status).to.equal(404);
  });

  it("deleting unknown account is handled", done => {
    agent
      .del(`/api/v1/accounts/9999999`)
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .end((delerr, delres) => {
        lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(delres);
        expect(delres.status).to.equal(404);
        done();
      });
  });

  after(async () => {
    agent.close();
  });
});
