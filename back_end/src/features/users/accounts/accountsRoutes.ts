import passport = require("passport");
import starterRouteProtection from "../../../middleware/starterRouteProtection";
import RouterConfiguration from "../../../routes/RouterConfiguration";
import profileModelMapper from "../profiles/profileModelMapper";
import * as accounts from "./accountsApiController";

const routerConfiguration = new RouterConfiguration("/accounts");

routerConfiguration.router.post("/signup", accounts.signup);
routerConfiguration.router.post("/login", accounts.login);
routerConfiguration.router.post("/logout", accounts.logout);
routerConfiguration.router.post("/changepassword", [
  starterRouteProtection.isAuthenticatedRouteProtection,
  accounts.changePassword
]);
routerConfiguration.router.post(
  "/activate/:activationCode",
  accounts.activateAccount
);
routerConfiguration.router.post(
  "/auth/google",
  passport.authenticate("google-token"),
  (req, res) => {
    res.status(200).json(profileModelMapper.map(req.user));
  }
);
routerConfiguration.router.delete("/:id", accounts.deleteById);

export default routerConfiguration;
