import { NextFunction, Request, Response } from "express";
import passport = require("passport");
import { PassportAuthenticatedRequest } from "PassportAuthenticatedRequest";
import { StandardRestApiResponse } from "../../sharedModels/StandardRestApiResponse";
import { IUserInstance } from "../datamodels/user";
import profileModelMapper from "../profiles/profileModelMapper";
import ISignupModel from "./apimodels/ISignupModel";
import authenticationService from "./authenticationService";
import inputModelParsers from "./requestBodyModelParsers";
import sessionCookieConfiguration from "./sessionCookieConfiguration";

const INVALID_ACTIVATION_KEY_MESSAGE = "Invalid activation key";
const LOGIN_FAILED_MESSAGE = "Login failed";
const USER_EXISTS_PARTIAL_MESSAGE = "User already exists";

export function login(req: Request, res: Response, next: NextFunction) {
  passport.authenticate("local", (err: any, user: IUserInstance, info: any) => {
    if (err || !user) {
      let errorMessage = LOGIN_FAILED_MESSAGE;
      info.message && (errorMessage = `${errorMessage} - ${info.message}`);
      return fullLogout(req, res, next, 400, errorMessage);
    }

    return req.logIn(user, loginErr => {
      if (loginErr) {
        return next(loginErr);
      }
      const returnableUser = profileModelMapper.map(user);
      return res.json(new StandardRestApiResponse(returnableUser));
    });
  })(req, res, next);
}

export function fullLogout(
  req: Request,
  res: Response,
  next: NextFunction,
  responseCode: number,
  message: string
) {
  req.logout();
  (req as any).session &&
    (req as any).session.destroy((err: any) => {
      if (!err) {
        return res
          .clearCookie(sessionCookieConfiguration.sessionCookieName, {
            path: sessionCookieConfiguration.sessionCookiePath
          })
          .status(responseCode)
          .json(new StandardRestApiResponse(null, message));
      }
      return next(err);
    });
}

export function logout(req: Request, res: Response, next: NextFunction) {
  return fullLogout(
    req,
    res,
    next,
    200,
    authenticationService.LOGGED_OUT_MESSAGE
  );
}

export async function signup(
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void | Response> {
  try {
    const userModel = req.body as ISignupModel;
    const returnableUser = await authenticationService.signUp(userModel);
    return res.status(200).json(new StandardRestApiResponse(returnableUser));
  } catch (error) {
    if ((error.message as string).includes(USER_EXISTS_PARTIAL_MESSAGE)) {
      return res
        .status(400)
        .json(new StandardRestApiResponse(null, error.message));
    } else {
      return next(error);
    }
  }
}

// Only exposed for current logged in user
export const changePassword = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const user = await authenticationService.changePassword(
      inputModelParsers.parseChangePasswordModel(req)
    );
    return res.status(200).send(new StandardRestApiResponse(user));
  } catch (error) {
    return next(error);
  }
};

export const activateAccount = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const user = await authenticationService.activateAccount(
      inputModelParsers.parseActivateAccountModel(req)
    );
    return res.status(200).send(new StandardRestApiResponse(user));
  } catch (error) {
    if (error.message === INVALID_ACTIVATION_KEY_MESSAGE) {
      return res
        .status(400)
        .json(
          new StandardRestApiResponse(null, INVALID_ACTIVATION_KEY_MESSAGE)
        );
    }

    return next(error);
  }
};

export const deleteById = async (
  req: PassportAuthenticatedRequest,
  res: Response,
  next: NextFunction
) => {
  try {
    const id = await authenticationService.deleteById(req.params.id, req.user);

    return res.status(200).json(new StandardRestApiResponse({ id }));
  } catch (err) {
    return next(err);
  }
};
