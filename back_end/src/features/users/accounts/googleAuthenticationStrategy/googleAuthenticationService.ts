import { db } from "../../../../datapersistence/DbInstance";
import { IUserInstance } from "../../datamodels/user";
import userPropertyNormalizers from "../../userPropertyNormalizers";
import { IGoogleProfile } from "../apimodels/IGoogleProfile";
import userModelValidator from "../validators/userModelValidator";
import { ExistingEmailError } from "./ExistingEmailError";

const handleGoogleAuthenticate = async (
  currentUser: IUserInstance,
  accessToken: string,
  refreshToken: string,
  googleProfile: IGoogleProfile
): Promise<IUserInstance | void> => {
  if (userIsLoggedWithDifferentAccount(currentUser)) {
    return attachGoogleAccount(currentUser, googleProfile);
  } else {
    return signUpWithGoogle(googleProfile);
  }
};

const signUpWithGoogle = async (
  googleProfile: IGoogleProfile
): Promise<IUserInstance> => {
  const existingGoogleUser = await db.User.findOne({
    where: { googleAccountId: googleProfile.id }
  });

  if (existingGoogleUser) {
    return existingGoogleUser;
  }

  const existingEmailUser = await db.User.findOne({
    where: { email: googleProfile.emails[0].value }
  });
  if (existingEmailUser) {
    throw new ExistingEmailError();
  }
  let newUser = await db.User.build();
  newUser = applyGoogleProfileToUserModel(newUser, googleProfile);

  userModelValidator.validate(newUser);

  const savedUser = await newUser.save();
  return savedUser;
};

const applyGoogleProfileToUserModel = (
  user: IUserInstance,
  googleProfile: IGoogleProfile
): IUserInstance => {
  user.googleAccountId = googleProfile.id;
  user.firstname = user.firstname || googleProfile.name.givenName;
  user.lastname = user.lastname || googleProfile.name.familyName;

  user.email =
    user.email ||
    userPropertyNormalizers.normalizeEmail(googleProfile.emails[0].value);

  user.username =
    user.username || userPropertyNormalizers.normalizeUsername(user.email);
  // newUser.tokens.push({ kind: "google", accessToken });
  // currentUser.tokens.push({ kind: "google", accessToken });
  // user.profile.gender = user.profile.gender || profile._json.gender;
  // user.profile.picture =
  // user.profile.picture || profile._json.image.url;
  return user;
};

const attachGoogleAccount = async (
  currentUser: IUserInstance,
  googleProfile: IGoogleProfile
): Promise<IUserInstance> => {
  if (alreadyAttached(currentUser, googleProfile)) {
    return currentUser;
  } else {
    applyGoogleProfileToUserModel(currentUser, googleProfile);

    const savedUser = await currentUser.save();
    return savedUser;
  }
};

const alreadyAttached = (
  user: IUserInstance,
  googleProfile: IGoogleProfile
): boolean => {
  return user.googleAccountId === googleProfile.id;
};

const userIsLoggedWithDifferentAccount = (user: IUserInstance): boolean => {
  return !!user && (user.id || 0) > 0;
};

const unlinkAccount = async (userId: number): Promise<IUserInstance> => {
  const existingUser = await db.User.findById(userId, { rejectOnEmpty: true });

  const nonNullExistingUser = existingUser!;
  nonNullExistingUser.googleAccountId = undefined;

  const savedUser = await nonNullExistingUser.save();
  return savedUser;
};

export default {
  attachGoogleAccount,
  handleGoogleAuthenticate,
  signUp: signUpWithGoogle,
  unlinkAccount
};
