export class ExistingSocialAccountError extends Error {
  constructor() {
    super(
      "There is already an account on here that belongs to you with this network. Sign in with that account or delete it, then link it with your current account."
    );
  }
}
