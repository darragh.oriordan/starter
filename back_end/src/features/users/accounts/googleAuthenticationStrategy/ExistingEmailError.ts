export class ExistingEmailError extends Error {
  constructor() {
    super(
      "There is already an account on here that uses this email address. Sign in with that account or delete it, then link it with your current account."
    );
  }
}
