const sessionCookieName = "sessionId";
const sessionCookiePath = "/";

export default { sessionCookieName, sessionCookiePath };
