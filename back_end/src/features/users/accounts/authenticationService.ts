import { db } from "../../../datapersistence/DbInstance";
import resourceProtectionRules from "../../../features/utils/resourceProtectionRules";
import promisifiedUserMethods from "../datamodels/promisifiedUserMethods";
import { IUserInstance } from "../datamodels/user";
import IProfileModel from "../profiles/apiModels/IProfileModel";
import userModelMapper from "../profiles/profileModelMapper";
import accountsInputModelValidationRules from "../userInputModelValidationRules";
import userInputModelValidationRules from "../userInputModelValidationRules";
import userPropertyNormalizers from "../userPropertyNormalizers";
import { IActivateAccountModel } from "./apimodels/IActivateAccountModel";
import { IChangePasswordModel } from "./apimodels/IChangePasswordModel";
import ISignupModel from "./apimodels/ISignupModel";
import userModelValidator from "./validators/accountInputModelValidator";

const LOGIN_FAILED_USERNAME_MESSAGE = "Login failed - Incorrect username";
const LOGIN_FAILED_PASSWORD_MESSAGE = "Login failed - Incorrect password";
const LOGGED_OUT_MESSAGE = "logged out!";

const signUp = async (signUpUser: ISignupModel): Promise<IProfileModel> => {
  signUpUser.email = userPropertyNormalizers.normalizeEmail(signUpUser.email);
  signUpUser.username = userPropertyNormalizers.normalizeUsername(
    signUpUser.username
  );

  userModelValidator.validate(signUpUser);

  const savedUser = await promisifiedUserMethods.registerAsync(signUpUser);
  return userModelMapper.map(savedUser as IUserInstance);
};

const activateAccount = async (
  model: IActivateAccountModel
): Promise<IProfileModel> => {
  return promisifiedUserMethods
    .activateAsync(model.username, model.password, model.activationCode)
    .then(activatedUser => activatedUser.save())
    .then(savedUser => userModelMapper.map(savedUser));
};

const changePassword = async (
  model: IChangePasswordModel
): Promise<IProfileModel> => {
  userInputModelValidationRules.validatePassword(model.newPassword);
  userInputModelValidationRules.validateConfirmPassword(
    model.newPasswordConfirm
  );
  userInputModelValidationRules.validatePasswordMatch(
    model.newPassword,
    model.newPasswordConfirm
  );
  const user = await db.User.findById(model.userId, {
    rejectOnEmpty: true
  });
  const foundUser = user!;
  const authenticatedUser = await promisifiedUserMethods.authenticateAsync(
    model.currentPassword,
    foundUser
  );
  const updatedPasswordUser = await promisifiedUserMethods.setPasswordAsync(
    model.newPassword,
    authenticatedUser
  );
  const savedUser = await updatedPasswordUser.save();
  return userModelMapper.map(savedUser);
};

const deleteById = async (
  id: string,
  currentUser: IUserInstance | undefined
): Promise<number> => {
  accountsInputModelValidationRules.validateUserId(id);
  resourceProtectionRules.isAuthorisedOwnerOrAdminValidator(
    Number.parseInt(id, 10),
    currentUser
  );
  const user = await db.User.findById(id, {
    rejectOnEmpty: true
  });
  const foundUser = user!;

  const userId = foundUser.id!;
  foundUser.destroy();
  return userId;
};

export default {
  LOGGED_OUT_MESSAGE,
  LOGIN_FAILED_PASSWORD_MESSAGE,
  LOGIN_FAILED_USERNAME_MESSAGE,
  activateAccount,
  changePassword,
  deleteById,
  signUp
};
