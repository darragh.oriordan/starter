import validator from "validator";
import { InputValidationError } from "../sharedModels/InputValidationError";

const INVALID_USER_ID_MESSAGE = "The id was not in the correct format";
const INVALID_EMAIL_MESSAGE = "The email was not in the correct format";
const MUST_SUPPLY_PASSWORD_MESSAGE = "You must supply a password";
const MUST_SUPPLY_CONFIRMATION_PASSWORD_MESSAGE =
    "You must supply a confirmation password";
const MUST_MATCH_PASSWORDS_MESSAGE = "The password and confirmation must match";
const INVALID_USERNAME_MESSAGE = "You must supply a username";
const INVALID_TANDC_MESSAGE = "You must agree to the terms and conditions";

const validateUserId = (id: string): void => {
    const idAsInt = Number.parseInt(id, 10);
    if (Number.isNaN(idAsInt) || idAsInt <= 0) {
        throw new InputValidationError(INVALID_USER_ID_MESSAGE);
    }
};

const validateEmail = (email: string): void => {
    if (!validator.isEmail(email)) {
        throw new InputValidationError(INVALID_EMAIL_MESSAGE);
    }
};

const validatePassword = (password: string): void => {
    if (!password || !(password.length > 0)) {
        throw new InputValidationError(MUST_SUPPLY_PASSWORD_MESSAGE);
    }
};
const validateConfirmPassword = (password: string): void => {
    if (!password || !(password.length > 0)) {
        throw new InputValidationError(
            MUST_SUPPLY_CONFIRMATION_PASSWORD_MESSAGE
        );
    }
};
const validateUsername = (username: string): void => {
    if (!username || !(username.length > 0)) {
        throw new InputValidationError(INVALID_USERNAME_MESSAGE);
    }
};

const validateTandCAcceptance = (agreeToTerms: boolean): void => {
    if (!agreeToTerms) {
        throw new InputValidationError(INVALID_TANDC_MESSAGE);
    }
};
const validatePasswordMatch = (
    password: string,
    passwordConfirm: string
): void => {
    if (password !== passwordConfirm) {
        throw new InputValidationError(MUST_MATCH_PASSWORDS_MESSAGE);
    }
};

export default {
    INVALID_EMAIL_MESSAGE,
    INVALID_TANDC_MESSAGE,
    INVALID_USERNAME_MESSAGE,
    INVALID_USER_ID_MESSAGE,
    MUST_MATCH_PASSWORDS_MESSAGE,
    MUST_SUPPLY_PASSWORD_MESSAGE,
    validateConfirmPassword,
    validateEmail,
    validatePassword,
    validatePasswordMatch,
    validateTandCAcceptance,
    validateUserId,
    validateUsername
};
