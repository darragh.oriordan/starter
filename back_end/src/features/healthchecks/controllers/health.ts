import { NextFunction, Request, Response } from "express";

import sequelize = require("sequelize");
import { db } from "../../../datapersistence/DbInstance";

export const database = async (req: Request, res: Response, next: NextFunction) => {
    try {
        await db.sequelize
            .query("SELECT 1", {
                plain: true,
                raw: true,
                type: sequelize.QueryTypes.SELECT
            });
        return res.header(
            "Cache-Control",
            "no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0")
            .header("Expires", "-1")
            .header("Pragma", "no-cache")
            .status(200)
            .end();
    }
    // tslint:disable-next-line:brace-style
    catch (err) {
        return next(err);
    }
};

export const web = (req: Request, res: Response, next: NextFunction) => {
    return res.status(200).send("web ok");
};

export const auth = (req: Request, res: Response, next: NextFunction) => {
    return res.status(200).send("auth ok");
};
