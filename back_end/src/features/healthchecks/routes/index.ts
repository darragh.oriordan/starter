// tslint:disable-next-line:import-name
import starterRouteProtection from "../../../middleware/starterRouteProtection";
import RouterConfiguration from "../../../routes/RouterConfiguration";
import * as health from "../controllers/health";

const routerConfiguration = new RouterConfiguration("/healthcheck");

routerConfiguration.router.get("/web", health.web);
routerConfiguration.router.get("/db", health.database);
routerConfiguration.router.get("/auth", [
    starterRouteProtection.isAuthenticatedRouteProtection,
    health.auth
]);

export default routerConfiguration;
