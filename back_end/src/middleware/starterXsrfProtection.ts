import csurf = require("csurf");
import { NextFunction, Request, Response } from "express";
const useSecureCookies = process.env.USE_SECURE_COOKIES === "true";
const useSameSiteCookies = process.env.USE_SAMESITE_COOKIES === "true";
const disableProtection = process.env.DISABLE_CSRF_PROTECTION === "true";

const RESPONSE_COOKIE_NAME = "STARTER_XSRF_TOKEN";
const REQUEST_HEADER_NAME = "X-CSRF-Token";
const CSURF_STORE_COOKIE_NAME = "csrf_token";

const setXsrfResponseCookie = () => {
    if (disableProtection) {
        return (req: Request, res: Response, next: NextFunction) => {
            next();
        };
    }

    return (req: Request, res: Response, next: NextFunction) => {
        res.cookie(RESPONSE_COOKIE_NAME, req.csrfToken(), {
            maxAge: 60 * 60 * 1000,
            sameSite: useSameSiteCookies,
            secure: useSecureCookies
        });
        next();
    };
};

export const getProtectionMiddleware = () => {
    if (disableProtection) {
        return (req: Request, res: Response, next: NextFunction) => {
            next();
        };
    }
    return csurf({
        cookie: {
            key: CSURF_STORE_COOKIE_NAME,
            sameSite: useSameSiteCookies,
            secure: useSecureCookies
        },
        value: (req: Request) => {
            const token = req.get(REQUEST_HEADER_NAME) || "";
            return token;
        }
    });
};

export default {
    REQUEST_HEADER_NAME,
    RESPONSE_COOKIE_NAME,
    getProtectionMiddleware,
    setXsrfResponseCookie
};
