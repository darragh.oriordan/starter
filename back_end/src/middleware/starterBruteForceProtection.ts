import { Request, Response } from "express";
import expressBrute from "express-brute";
import expressBruteStoreSequelize = require("express-brute-store-sequelize");
import { db } from "../datapersistence/DbInstance";

function configure() {
  // There is an issue with the sequelize migration where it
  // adds an id column and the timestamp columns to the table generated by the brutestore
  // You can log in to the data base and hack the table
  // directly to remove this. but there should be a better way.

  // This would almost certainly be handled by a reverse proxy in real
  // production environment but we are bootstrapping as cheaply as possible!
  const failCallback = (
    req: Request,
    res: Response,
    // tslint:disable-next-line: ban-types
    next: Function,
    nextValidRequestDate: Date
  ) => {
    return res.status(429).json({
      // tslint:disable-next-line:prefer-template
      message:
        "You've made too many requests" +
        " in a short period of time, please try again after " +
        Math.ceil(
          (nextValidRequestDate.getTime() - new Date().getTime()) / 60000
        ) +
        " minute(s) (after " +
        nextValidRequestDate.toLocaleString() +
        ")"
    });
  };

  const handleStoreError = (error: any) => {
    throw new Error(`Couldn't configure the store. ${error.message}`);
  };

  const store = new expressBruteStoreSequelize(db.sequelize, {
    modelOptions: { freezeTableName: true },
    tableName: "BruteStore"
  });
  // Limit calls per hour per IP
  return new expressBrute(store, {
    attachResetToRequest: false,
    failCallback,
    freeRetries: 20,
    handleStoreError,
    lifetime: 60 * 3, // 1 hour (seconds not milliseconds)
    refreshTimeoutOnRequest: false
  });
}

export default configure;
