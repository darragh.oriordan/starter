import { ApolloServer } from "apollo-server-express";
import { db } from "../datapersistence/DbInstance";
import { IDb } from "../datapersistence/IDb";
import { IUserInstance } from "../features/users/datamodels/user";
// tslint:disable-next-line:import-name
import schema from "../graphql/schema";

export interface IStarterContext {
    db: IDb;
    user: IUserInstance;
}

const apollo = new ApolloServer({
    context: (req: any): IStarterContext => {
        return {
            db,
            user: req.req.user
        };
    },
    introspection: true,
    playground: false,
    schema
});

export default apollo;
