import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import chaiHttp from "chai-http";
import http from "http";
import { before } from "mocha";
import { Server } from "../express/serverInstance";
import ISignupModel from "../features/users/accounts/apimodels/ISignupModel";
import starterXsrfProtection from "../middleware/starterXsrfProtection";
import cookieParser from "../util/cookieParser";

chai.use(chaiAsPromised);
chai.use(chaiHttp);
let server: http.Server;
let agent: ChaiHttp.Agent;
let lastResponsesXsrfKey: string;
let createdUserId: number;
const goodUserModel = {
  agreeToTerms: true,
  email: "acsrf.user@gmail.com",
  firstname: "darragh",
  lastname: "oriordan",
  password: "superpassword",
  passwordConfirm: "superpassword",
  username: "acsrf.user@gmail.com"
} as ISignupModel;
describe("XSRF proteection is working", () => {
  before(async () => {
    server = await new Server().getHttpServer();
    agent = chai.request.agent(server);

    await agent.get("/").then(res => {
      lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(res);
    });
  });

  it("signing up with csrf will work", done => {
    agent
      .post("/api/v1/accounts/signup")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey)
      .send(goodUserModel)
      .end((err, res) => {
        chai.should().not.exist(err);
        chai.should().exist(res);
        res.status.should.equal(200);
        createdUserId = res.body.data.id;
        done();
      });
  });

  it("signing up should fail on csrf missing", done => {
    agent
      .post("/api/v1/accounts/signup")
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, "someinvalidtoken")
      .send(goodUserModel)
      .set("Content-Type", "application/json")
      .end((err, res) => {
        lastResponsesXsrfKey = cookieParser.parseXsrfTokenCookie(res);
        chai.should().not.exist(err);
        chai.should().exist(res);
        res.status.should.equal(403);
        res.body.message.should.equal("csrf token error");
        done();
      });
  });

  after(async () => {
    agent
      .del(`/api/v1/accounts/${createdUserId}`)
      .set(starterXsrfProtection.REQUEST_HEADER_NAME, lastResponsesXsrfKey);
    agent.close();
  });
});
