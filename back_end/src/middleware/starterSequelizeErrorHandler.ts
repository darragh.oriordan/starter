import { NextFunction, Request, Response } from "express";
import {
  ConnectionError,
  DatabaseError,
  EmptyResultError,
  UniqueConstraintError,
  ValidationError
} from "sequelize";
export const ALREADY_EXISTS_MESSAGE = "Could not save. Already exists";
export const MODEL_VALIDATION_MESSAGE = "There were validation errors";
export const DATABASE_CONNECTION_ERROR_MESSAGE =
  "Could not connect to database";
export const DATABASE_ERROR_MESSAGE =
  "There was a problem processing the request";
export const NOT_FOUND_MESSAGE = "Not found";

const handleSequelizeErrors = () => {
  return (err: any, req: Request, res: Response, next: NextFunction) => {
    if (err instanceof UniqueConstraintError) {
      return res.status(403).json({ message: ALREADY_EXISTS_MESSAGE });
    }
    if (err instanceof ValidationError) {
      return res.status(403).json({
        errors: (err as ValidationError).errors.map((se: any) => se.message),
        message: MODEL_VALIDATION_MESSAGE
      });
    }
    if (err instanceof ConnectionError) {
      return res
        .status(500)
        .json({ message: DATABASE_CONNECTION_ERROR_MESSAGE });
    }
    if (err instanceof DatabaseError) {
      return res.status(500).json({
        message: DATABASE_ERROR_MESSAGE
      });
    }
    if (err instanceof EmptyResultError) {
      return res.status(404).json({ message: NOT_FOUND_MESSAGE });
    }
    next(err);
    return;
  };
};

export default handleSequelizeErrors;
