import { NextFunction, Request, Response } from "express";

function isDeleteMethodWithBodyPresent(req: Request): boolean {
    return (
        req.method === "DELETE" &&
        req.body &&
        req.body.constructor === Object &&
        Object.keys(req.body).length > 0
    );
}

function isPostMethod(req: Request): boolean {
    return req.method === "POST";
}

function isPutMethod(req: Request): boolean {
    return req.method === "POST";
}

function isJsonContentType(req: Request): boolean {
    return !!req.is("application/json");
}
export default function validateMethodAndContentType() {
    return (req: Request, res: Response, next: NextFunction) => {
        if (
            (isPostMethod(req) ||
                isPutMethod(req) ||
                isDeleteMethodWithBodyPresent(req)) &&
            !isJsonContentType(req)
        ) {
            return res.status(406).json({
                message:
                    "Content type rejected. This endpoint only accepts application/json"
            });
        }
        next();
        return;
    };
}
