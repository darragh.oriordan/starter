import { NextFunction, Request, Response } from "express";
import { InputValidationError } from "../features/sharedModels/InputValidationError";
import { ResourceAuthorizationError } from "../features/utils/ResourceAuthorizationError";

const handleApplicationErrors = () => {
  return [
    // resource access error
    (err: any, req: Request, res: Response, next: NextFunction) => {
      if (err instanceof ResourceAuthorizationError) {
        return res.status(401).json({ message: err.message });
      }
      return next(err);
    },
    // input validation error
    (err: any, req: Request, res: Response, next: NextFunction) => {
      if (err instanceof InputValidationError) {
        return res.status(400).json({ message: err.message });
      }
      return next(err);
    },
    // csrf error handler
    (err: any, req: Request, res: Response, next: NextFunction) => {
      if (err.code !== "EBADCSRFTOKEN") {
        return next(err);
      }

      // handle CSRF token errors here
      res.status(403).json({ message: "csrf token error" });
    },
    // All other errors
    (err: any, req: Request, res: Response, next: NextFunction) => {
      if (process.env.NODE_ENV === "development") {
        // tslint:disable-next-line: no-console
        console.error(err);
        res.status(500).json({
          message: err.message,
          stack: err.stack
        });
      } else {
        // log error to some service
        res.status(500).json({ message: "Server Error" });
      }
    }
  ];
};

export default handleApplicationErrors;
