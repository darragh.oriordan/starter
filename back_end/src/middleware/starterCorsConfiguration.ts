import cors from "cors";

const starterCorsProtection = () => {
    const whitelist = getList();
    const corsOptions = {
        credentials: true,
        optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
        origin: whitelist
    };

    return cors(corsOptions);
};

const getList = (): string[] => {
    const corsSetting = process.env.CORS_ALLOWED_HOSTS || "";
    return corsSetting.split(",");
};

export default starterCorsProtection;
