import connectSessionSequelize from "connect-session-sequelize";
import cookieParser from "cookie-parser";
import { Request } from "express";
import expressSession from "express-session";
import passport from "passport";
import passportGoogleToken from "passport-google-token";
import { db } from "../datapersistence/DbInstance";
import { IGoogleProfile } from "../features/users/accounts/apimodels/IGoogleProfile";
import googleAuthenticationService from "../features/users/accounts/googleAuthenticationStrategy/googleAuthenticationService";
import sessionCookieConfiguration from "../features/users/accounts/sessionCookieConfiguration";
import { IUserInstance } from "../features/users/datamodels/user";

const useSecureCookies = process.env.USE_SECURE_COOKIES === "true";
const useSameSiteCookies = process.env.USE_SAMESITE_COOKIES === "true";

const googleTokenStrategy = new passportGoogleToken.Strategy(
  {
    clientID: process.env.GOOGLE_CLIENT_ID || "",
    clientSecret: process.env.GOOGLE_CLIENT_SECRET || "",
    passReqToCallback: true
  },
  (
    req: Request,
    accessToken: string,
    refreshToken: string,
    profile: IGoogleProfile,
    done: (error: Error | null, user?: any) => void
  ) => {
    return googleAuthenticationService
      .handleGoogleAuthenticate(req.user, accessToken, refreshToken, profile)
      .catch((error: Error) => done(error))
      .then((userInstance: IUserInstance | void) => done(null, userInstance));
  }
);

const sequelizeSessionStoreInstance = connectSessionSequelize(
  expressSession.Store
);

function configurePassport() {
  const sessionSecret = process.env.SESSION_SECRET;
  if (!sessionSecret || sessionSecret.length === 0) {
    throw new Error(
      "The session secret parameter must be set to configure passport with sessions"
    );
  }
  const dbUser = db.User;

  passport.use(dbUser.createStrategy());
  passport.use(googleTokenStrategy);
  passport.serializeUser(dbUser.serializeUser());
  passport.deserializeUser(dbUser.deserializeUser());
  return [
    cookieParser(),
    expressSession({
      cookie: {
        httpOnly: true,
        maxAge: 1000 * 60 * 60 * 24, // 24 hours
        sameSite: useSameSiteCookies,
        secure: useSecureCookies
      },
      name: sessionCookieConfiguration.sessionCookieName,
      resave: false,
      saveUninitialized: true,
      secret: sessionSecret,
      store: new sequelizeSessionStoreInstance({
        db,
        table: "Session"
      })
    }),
    passport.initialize(),
    passport.session()
  ];
}

export default {
  configurePassport
};
