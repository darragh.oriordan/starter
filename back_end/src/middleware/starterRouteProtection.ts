import { NextFunction, Response } from "express";
import { PassportAuthenticatedRequest } from "PassportAuthenticatedRequest";
import rolesUtility, { RoleNames } from "../features/users/rolesUtility";

const ENDPOINT_ACCESS_ERROR_MESSAGE =
  "You do not have permission to access this resource";

function isAdministratorRouteProtection(
  req: PassportAuthenticatedRequest,
  res: Response,
  next: NextFunction
) {
  if (req.user && rolesUtility.isInRole(RoleNames.Administrator, req.user)) {
    next();
  } else {
    res.status(401).json({
      message: "Unauthorised, you must login to use this api"
    });
  }
}

function isAuthenticatedRouteProtection(
  req: PassportAuthenticatedRequest,
  res: Response,
  next: NextFunction
) {
  if (req.user) {
    next();
  } else {
    res.status(401).json({
      message:
        "Unauthorised, you must login to use this api - [baseurl]/auth/login"
    });
  }
}

export default {
  ENDPOINT_ACCESS_ERROR_MESSAGE,
  isAdministratorRouteProtection,
  isAuthenticatedRouteProtection
};
