import { NextFunction, Request, Response } from "express";

import connectTimeout from "connect-timeout";

export function handleHaltOnTimedout() {
    return (
        req: Request,
        res: Response,
        next: NextFunction
    ): Response | void => {
        if (req.timedout) {
            return res.status(429).send({ message: "Request timed out" });
        }

        next();
    };
}

function config() {
    return connectTimeout("3000");
}

export default config;
