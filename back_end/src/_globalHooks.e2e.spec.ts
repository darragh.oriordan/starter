import { after, before } from "mocha";
import { db } from "./datapersistence/DbInstance";
import { down, up } from "./datapersistence/seeders/20180903081830-test-users";

before(async () => {
    await down(db.sequelize.getQueryInterface(), db.Sequelize);
    await up(db.sequelize.getQueryInterface(), db.Sequelize);
});

after(async () => {
    await down(db.sequelize.getQueryInterface(), db.Sequelize);
});
