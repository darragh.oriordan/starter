import { gql } from "apollo-server-core";
import { IResolvers, makeExecutableSchema } from "graphql-tools";
import { merge } from "lodash";
import { resolvers as userResolvers } from "../features/users/profiles/graphql/resolvers";
import { typeDefs as userTypeDefs } from "../features/users/profiles/graphql/typeDefs";
import DateTime from "../graphql/CustomScalars/DateTime";

const root = gql`
    scalar DateTime

    type Query {
        _: String
    }

    type Mutation {
        _: String
    }

    type Subscription {
        _: String
    }
`;

const customScalars = {
    DateTime
};

const mergedResolvers: IResolvers = merge({}, customScalars, userResolvers);

const mergedTypeDefs = [root, userTypeDefs];

const schema = makeExecutableSchema({
    allowUndefinedInResolve: false,
    resolvers: mergedResolvers,
    typeDefs: mergedTypeDefs
});

export default schema;
