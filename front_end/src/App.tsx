import * as React from "react";
import { Route } from "react-router-dom";
import "./App.css";
import MarkerProperties from "./models/MarkerProperties";

import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Pulsemap from "./pages/Pulsemap";
import Signup from "./pages/Signup";

interface IAppState {
  markers: MarkerProperties[];
}
class App extends React.Component<{}, IAppState> {
  constructor(props: {}) {
    super(props);
  }

  public render() {
    return (
      <div className="App">
        <Route exact={true} path={`/vis/pulse`} component={Pulsemap} />
        <Route exact={true} path={`/`} component={Home} />

        <Route exact={true} path={`/signup`} component={Signup} />
        <Route exact={true} path={`/login`} component={Login} />
        <Route exact={true} path={`/logout`} component={Logout} />
      </div>
    );
  }
}

export default App;
