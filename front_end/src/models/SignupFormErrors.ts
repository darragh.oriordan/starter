export default class SignupFormErrors {
    public firstname: boolean = false;
    public lastname: boolean = false;
    public email: boolean = false;
    public password: boolean = false;
    public confirmPassword: boolean = false;
    public agreeToTerms: boolean = false;
    public form: boolean = false;
    public messages: string[] = [];
}