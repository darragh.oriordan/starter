export default class StatItemConfiguration {
    constructor(       
        public headerClasses: string,        
        public headerText: string,
        public valueClasses: string,
        public valueText: string,
        public wrapperClasses: string) {

    }
}