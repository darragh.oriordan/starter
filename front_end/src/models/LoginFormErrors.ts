export default class LoginFormErrors {
    public email: boolean = false;
    public password: boolean = false;
    public form: boolean = false;
    public messages: string[] = [];
}