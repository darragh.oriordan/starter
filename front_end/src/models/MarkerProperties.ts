class MarkerProperties {
    constructor(public position: { lat: number, lng: number }, public labelClass: string) {
    }
}

export default MarkerProperties