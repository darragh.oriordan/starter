const styles = {
    darkPulse: [

        {
            "elementType": 'labels.text.fill',
            "featureType": "all",

            "stylers": [
                {
                    "color": '#ffffff'
                }
            ]
        },
        {
            "elementType": 'labels.text.stroke',
            "featureType": 'all',

            "stylers": [
                {
                    "color": '#000000'
                },
                {
                    "lightness": 13
                }
            ]
        },
        {
            elementType: 'labels',
            featureType: 'poi',

            stylers: [
                { visibility: 'off' }
            ]
        }
        , {
            "elementType": 'labels',
            "featureType": 'road',

            "stylers": [
                { "visibility": 'off' }
            ]
        }
        , {
            "elementType": 'labels.text',
            "stylers": [
                { "visibility": 'off' }
            ]
        }
        ,
        {
            "elementType": 'geometry.fill',
            "featureType": 'administrative',

            "stylers": [
                {
                    "color": '#000000'
                }
            ]
        },
        {
            "elementType": 'geometry.stroke',
            "featureType": 'administrative',

            "stylers": [
                {
                    "color": '#144b53'
                },
                {
                    "lightness": 14
                },
                {
                    "weight": 1.4
                }
            ]
        },
        {
            "elementType": 'all',
            "featureType": 'landscape',

            "stylers": [
                {
                    "color": '#08304b'
                }
            ]
        },
        {
            "elementType": 'geometry',
            "featureType": 'poi',

            "stylers": [
                {
                    "color": '#0c4152'
                },
                {
                    "lightness": 5
                }
            ]
        },
        {
            "elementType": 'geometry.fill',
            "featureType": 'road.highway',

            "stylers": [
                {
                    "color": '#000000'
                }
            ]
        },
        {
            "elementType": 'geometry.stroke',
            "featureType": 'road.highway',

            "stylers": [
                {
                    "color": '#0b434f'
                },
                {
                    "lightness": 25
                }
            ]
        },
        {
            "elementType": 'geometry.fill',
            "featureType": 'road.arterial',

            "stylers": [
                {
                    "color": '#000000'
                }
            ]
        },
        {
            "elementType": 'geometry.stroke',
            "featureType": 'road.arterial',

            "stylers": [
                {
                    "color": '#0b3d51'
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "elementType": 'geometry',
            "featureType": 'road.local',

            "stylers": [
                {
                    "color": '#000000'
                }
            ]
        },
        {
            "elementType": 'all',
            "featureType": 'transit',

            "stylers": [
                {
                    "color": '#146474'
                }
            ]
        },
        {
            "elementType": 'all',
            "featureType": 'water',

            "stylers": [
                {
                    "color": '#072232'
                }
            ]
        }
    ]
}

export default styles;