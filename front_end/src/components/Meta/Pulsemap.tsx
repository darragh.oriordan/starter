
import * as React from 'react';
import { Helmet } from "react-helmet";

export default () => (
    <Helmet>
        <title>Pulse map</title>
    </Helmet>
)