
import * as React from 'react';
import { Helmet } from "react-helmet";

export default () => (
    <Helmet>
        <title>Home</title>
    </Helmet>
)