
import * as React from 'react';

import { Helmet } from "react-helmet";
import MarkerProperties from '../models/MarkerProperties';
import StatItemConfiguration from '../models/StatItemConfiguration';
// tslint:disable-next-line
import mapThemes from "../utils/mapThemes";
import './Pulsemap.css';
// this.map.set("styles",

interface IPulsemapProps {
    statsConfiguration?: StatItemConfiguration[];
    logoPath?: string;
    logoClasses?: string;
    defaultMapCenter?: { lat: number, lng: number };
    defaultMapZoom?: number;
    mapTheme?: Array<{}>;
    markers?: MarkerProperties[];
    mapsUrl?: string;
}

interface IPulsemapState {
    map: google.maps.Map | undefined;
    date: Date;
}

class Pulsemap extends React.Component<IPulsemapProps, IPulsemapState> {
    public static defaultProps: Partial<IPulsemapProps> = {
        defaultMapCenter: { lat: -41.28, lng: 174.77 },
        defaultMapZoom: 6,
        logoClasses: "logoHolder",
        logoPath: "/images/pulsemap/logo.png",
        mapTheme: mapThemes.darkPulse,
        mapsUrl: "https://maps.googleapis.com/maps/api/js?key=AIzaSyD6tEnbFQW0PK5sCetFQF3b9L54GANhFic&v=3.exp&libraries=geometry,drawing,places",

        statsConfiguration: [new StatItemConfiguration(
            "statsHeaderText",
            "dd MMMM yyyy",
            "statsValueText",
            "HH:mm:ss",
            "statsItem timeStat"
        ),
        new StatItemConfiguration(

            "statsHeaderText",
            "Items Sold",
            "statsValueText",
            "Starting...",
            "statsItem itemsSoldStat"
        ),
        new StatItemConfiguration(

            "statsHeaderText",
            "Items Listed",
            "statsValueText",
            "Starting...",
            "statsItem timeStat"
        )
        ]
    }
    public map: google.maps.Map
    public tickTimer: any
    private mapCanvas: React.RefObject<HTMLDivElement>;
    constructor(props: IPulsemapProps) {
        super(props)


        this.state = {
            date: new Date(),
            map: undefined,

        }

        this.mapCanvas = React.createRef<HTMLDivElement>();
    }


    public componentWillUnmount() {
        clearInterval(this.tickTimer);
    }

    public tick() {
        const limit = 200;
        let i = 0;

        for (i = 0; i < limit; i++) {
            this.addMarker(new google.maps.LatLng((this.props.defaultMapCenter || {} as any).lat + (i / 100) + (Math.random()), (this.props.defaultMapCenter || {} as any).lng + (i / 100) + Math.random()));
        }

        this.setState({
            date: new Date()
        });
    }
    public componentDidMount() {
        // Connect the initMap() function within this class to the global window context,
        // so Google Maps can invoke it
        const mapOptions: google.maps.MapOptions = {
            center: this.props.defaultMapCenter,
            styles: this.props.mapTheme,
            zoom: this.props.defaultMapZoom
        };
        (window as any).initMap = () => this.initMap(mapOptions);
        // Asynchronously load the Google Maps script, passing in the callback reference
        this.loadJS(this.props.mapsUrl + '&callback=initMap')
        this.tickTimer = setInterval(
            () => this.tick(),
            1000
        );
    }

    // Add a marker to the map and push to the array.
    public addMarker(location: google.maps.LatLng) {

        if (document.hidden) {
            return;
        }

        const bounds: google.maps.LatLngBounds = this.map.getBounds() || {} as google.maps.LatLngBounds;

        if (!bounds.contains(new google.maps.LatLng(location.lat(), location.lng()))) {
            return;
        }

        const classString = "mapPointPulse ";
        const animlength = 1500;
        const pulseRadius = 15;
        const marker: any = new (MarkerWithLabel as any)({
            clickable: false,
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 0
            },
            labelAnchor: new google.maps.Point(pulseRadius, pulseRadius),
            labelClass: classString,
            position: location,
        });
        marker.setMap(this.map as any);
        marker.labelStyle["-webkit-animation-duration"] = (animlength / 1000).toFixed(1) + "s"
        marker.labelStyle["-moz-animation-duration"] = (animlength / 1000).toFixed(1) + "s"
        marker.labelStyle["-o-animation-duration"] = (animlength / 1000).toFixed(1) + "s"
        marker.labelStyle["animation-duration"] = (animlength / 1000).toFixed(1) + "s"
        marker.labelStyle.background = "#00FFCD"
        marker.labelStyle.height = (pulseRadius * 2).toFixed(1) + "px";
        marker.labelStyle.width = (pulseRadius * 2).toFixed(1) + "px";
        marker.labelStyle.borderRadius = (pulseRadius).toFixed(1) + "px";
        const timeout = setTimeout(() => {
            (marker as any).setMap(null);

            clearTimeout(timeout);

        }, 1500);

    }

    public initMap(mapOptions: google.maps.MapOptions) {
       this.loadJS("/scripts/markerwithlabel.js")
        const mapNode = this.mapCanvas.current;
        mapOptions.mapTypeId = google.maps.MapTypeId.ROADMAP;
        if (mapNode) {
            this.map = new google.maps.Map(mapNode, mapOptions);
        }
    }

    public loadJS(src: string) {
        const ref = window.document.getElementsByTagName("script")[0];
        const script = window.document.createElement("script");
        script.src = src;
        script.async = true;
        (ref.parentNode || {} as Node).insertBefore(script, ref);

    }

    public render() {
        return (
            <div>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>My Title</title>
                    <link rel="canonical" href="http://mysite.com/example" />
                </Helmet>
                <div className={this.props.logoClasses}>
                    <img src={this.props.logoPath} />
                </div>
                <div ref={this.mapCanvas} className="map-canvas" />

                <div className="statsHolder">
                    {(this.props.statsConfiguration || []).map((element, index) =>
                        (<div className={element.wrapperClasses} key={index.toString()}>
                            <span className={element.headerClasses}>{element.headerText}</span>
                            <span className={element.valueClasses}>{element.valueText}</span>
                        </div>)
                    )}
                </div>
            </div>
        );
    }
}

 declare function MarkerWithLabel(opts: any): void;

export default Pulsemap