import axios from "axios";
import * as React from "react";
import { Container, Form } from "semantic-ui-react";

class LogoutForm extends React.Component<{}> {
  constructor(props: {}) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  public render() {
    return (
      <Container>
        <Form size={"big"} onSubmit={this.handleSubmit}>
          <Form.Button>Confirm Logout</Form.Button>
        </Form>
      </Container>
    );
  }

  private handleSubmit(event: React.FormEvent<EventTarget>) {
    event.preventDefault();
    // post
    const axiosInstance = axios.create({
      validateStatus: status => {
        return status === 200;
      }
    });

    axiosInstance
      .post(
        "/api/v1/accounts/logout",
        {},
        {
          withCredentials: true,
          xsrfCookieName: "STARTER_XSRF_TOKEN",
          xsrfHeaderName: "X-CSRF-Token"
        }
      )
      .then(() => {
        // tslint:disable-next-line: no-console
        console.log("logged out");
      })
      .catch(error => {
        // tslint:disable-next-line:no-console
        console.log(error);
      });
  }
}

export default LogoutForm;
