import axios from "axios";
import * as React from "react";
import { Button, Container, Form, Message } from "semantic-ui-react";
import LoginFormErrors from "src/models/LoginFormErrors";
import LoginFormValidator from "src/validation/LoginFormValidator";

export interface ILoginFormState {
  email: string;
  password: string;
  errors: LoginFormErrors;
  success: boolean;
  authCheckMessage: string;
}

class LoginForm extends React.Component<{}, ILoginFormState> {
  private formValidator: LoginFormValidator;

  constructor(props: {}) {
    super(props);
    this.state = {
      authCheckMessage: "",
      email: "",
      errors: new LoginFormErrors(),
      password: "",
      success: false
    };

    this.formValidator = new LoginFormValidator();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.authCheck = this.authCheck.bind(this);
  }

  public render() {
    return (
      <Container>
        <Form
          size={"big"}
          onSubmit={this.handleSubmit}
          error={this.state.errors.form}
          success={this.state.success}
        >
          <Message
            error={true}
            header="Oops!"
            list={this.state.errors.messages}
          />
          <Message
            success={true}
            header="Logged in!"
            content="redirecting...."
          />

          <Form.Input
            fluid={true}
            label="Email"
            name="email"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleChange}
            required={true}
            error={this.state.errors.email}
          />

          <Form.Input
            fluid={true}
            label="Password"
            type="password"
            name="password"
            placeholder="Password"
            value={this.state.password}
            onChange={this.handleChange}
            required={true}
            error={this.state.errors.password}
          />

          <Form.Button>Login</Form.Button>
        </Form>

        <Button onClick={this.authCheck}>now check auth</Button>
        <p> {this.state.authCheckMessage}</p>
      </Container>
    );
  }
  private async authCheck(event: any) {
    event.preventDefault();
    try {
      const axiosInstance = axios.create({
        validateStatus: status => {
          return status === 200;
        }
      });
      const response = await axiosInstance.get("/api/v1/healthcheck/auth", {
        withCredentials: true
      });

      this.setState({
        authCheckMessage: response.data
      });
    } catch (error) {
      this.setState({
        authCheckMessage: "auth failed"
      });
      const reqErrors = new LoginFormErrors();
      reqErrors.form = true;
      reqErrors.messages.push(
        error.response && error.response.data && error.response.data.message
      );
      this.setState(current => ({ ...current, errors: reqErrors }));
    }
  }
  private handleChange(event: React.FormEvent<EventTarget>) {
    const target = event.target as HTMLInputElement;

    const newState = Object.assign({}, this.state);
    newState[target.name] = target.value;

    this.setState(newState);
  }
  // private readCookie(name: string) {
  //     const nameEQ = name + "=";
  //     const ca = document.cookie.split(';');
  //     // tslint:disable-next-line:prefer-for-of
  //     for (let i = 0; i < ca.length; i++) {
  //         let c = ca[i];
  //         while (c.charAt(0) === ' ') { c = c.substring(1, c.length); }
  //         if (c.indexOf(nameEQ) === 0) { return c.substring(nameEQ.length, c.length); }
  //     }
  //     return null;
  // }
  private handleSubmit(event: React.FormEvent<EventTarget>) {
    event.preventDefault();

    const errors = this.formValidator.validate(this.state);
    if (errors.form) {
      this.setState(current => ({ ...current, success: false, errors }));
      return;
    }
    // post
    const axiosInstance = axios.create({
      validateStatus: status => {
        return status === 200;
      }
    });

    // axios.defaults.xsrfHeaderName = "X-CSRF-Token";
    // axios.defaults.xsrfCookieName = "STARTER_XSRF_TOKEN";
    axiosInstance
      .post(
        "/api/v1/accounts/login",
        { username: this.state.email, password: this.state.password },
        {
          withCredentials: true,
          xsrfCookieName: "STARTER_XSRF_TOKEN",
          xsrfHeaderName: "X-CSRF-Token"
        }
      )
      .then(response => {
        // clear form if successful
        this.setState({
          email: "",
          errors: new LoginFormErrors(),
          password: "",
          success: true
        });
      })
      .catch(error => {
        const reqErrors = new LoginFormErrors();
        reqErrors.form = true;

        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          reqErrors.messages.push(
            error.response && error.response.data && error.response.data.message
          );
          this.setState(current => ({
            ...current,
            errors: reqErrors,
            success: false
          }));
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          // tslint:disable-next-line:no-console
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          // tslint:disable-next-line:no-console
          console.log("Error", error.message);
        }
        // tslint:disable-next-line:no-console
        console.log(error.config);
      });
  }
}

export default LoginForm;
