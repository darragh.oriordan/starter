import * as React from "react";
import { Link } from "react-router-dom";
import { Container, Dropdown, Image, Menu } from "semantic-ui-react";

const MainMenu = () => (
  <Menu fixed="top" inverted={true} stackable={true}>
    <Container>
      <Menu.Item as="a" header={true}>
        <Image
          size="mini"
          src="/images/logo.jpg"
          style={{ marginRight: "1.5em" }}
        />
        Project Name
      </Menu.Item>
      <Menu.Item as={Link} name="home" to="/">
        Home
      </Menu.Item>
      <Menu.Item as={Link} name="pulse" to="/vis/pulse">
        Pulse
      </Menu.Item>

      <Dropdown item={true} simple={true} text="Dropdown">
        <Dropdown.Menu>
          <Dropdown.Item>List Item</Dropdown.Item>
          <Dropdown.Item>List Item</Dropdown.Item>
          <Dropdown.Divider />
          <Dropdown.Header>Header Item</Dropdown.Header>
          <Dropdown.Item>
            <i className="dropdown icon" />
            <span className="text">Submenu</span>
            <Dropdown.Menu>
              <Dropdown.Item>List Item</Dropdown.Item>
              <Dropdown.Item>List Item</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown.Item>
          <Dropdown.Item>List Item</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
      <Menu.Item as={Link} name="signup" to="/signup">
        Signup
      </Menu.Item>
      <Menu.Item as={Link} name="login" to="/login">
        Login
      </Menu.Item>
      <Menu.Item as={Link} name="logout" to="/logout">
        Logout
      </Menu.Item>
    </Container>
  </Menu>
);

export default MainMenu;
