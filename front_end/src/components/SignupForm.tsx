import axios from "axios";
import * as React from "react";
import { GoogleLogin } from "react-google-login";
import { Container, Form, Message } from "semantic-ui-react";
import SignupFormErrors from "src/models/SignupFormErrors";
import SignupFormValidator from "src/validation/SignupFormValidator";
export interface ISignupFormState {
  firstname: string;
  lastname: string;
  email: string;
  password: string;
  confirmPassword: string;
  agreeToTerms: boolean;
  errors: SignupFormErrors;
  success: boolean;
}

class SignupForm extends React.Component<{}, ISignupFormState> {
  private signupformValidator: SignupFormValidator;

  constructor(props: {}) {
    super(props);
    this.state = {
      agreeToTerms: false,
      confirmPassword: "",
      email: "",
      errors: new SignupFormErrors(),
      firstname: "",
      lastname: "",
      password: "",
      success: false
    };

    this.signupformValidator = new SignupFormValidator();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  public render() {
    return (
      <Container>
        <Form
          size={"big"}
          onSubmit={this.handleSubmit}
          error={this.state.errors.form}
          success={this.state.success}
        >
          <Message
            error={true}
            header="Oops!"
            list={this.state.errors.messages}
          />
          <Message
            success={true}
            header="Sign up completed"
            content="You're all signed up!"
          />
          <Form.Group widths="equal">
            <Form.Input
              fluid={true}
              label="First name"
              name="firstname"
              placeholder="First name"
              value={this.state.firstname}
              onChange={this.handleChange}
              required={true}
              error={this.state.errors.firstname}
            />
            <Form.Input
              fluid={true}
              label="Last name"
              name="lastname"
              placeholder="Last name"
              value={this.state.lastname}
              onChange={this.handleChange}
              required={true}
              error={this.state.errors.lastname}
            />
          </Form.Group>
          <Form.Input
            fluid={true}
            label="Email"
            name="email"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleChange}
            required={true}
            error={this.state.errors.email}
          />
          <Form.Group widths="equal">
            <Form.Input
              fluid={true}
              label="Password"
              type="password"
              name="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.handleChange}
              required={true}
              error={this.state.errors.password}
            />
            <Form.Input
              fluid={true}
              label="Confirm Password"
              type="password"
              name="confirmPassword"
              placeholder="Confirm Password"
              value={this.state.confirmPassword}
              onChange={this.handleChange}
              required={true}
              error={this.state.errors.confirmPassword}
            />
          </Form.Group>

          <Form.Checkbox
            label="I agree to the Terms and Conditions"
            name="agreeToTerms"
            checked={this.state.agreeToTerms}
            // tslint:disable-next-line:jsx-no-lambda
            onChange={(e, { checked }) => this.handleTermsChange(checked)}
            required={true}
            error={this.state.errors.agreeToTerms}
          />

          <Form.Button>Submit</Form.Button>
        </Form>

        <GoogleLogin
          clientId="59878836733-7ekolc5pbpmrn164vb2qccsimulruinm.apps.googleusercontent.com"
          buttonText="Continue with Google..."
          onSuccess={this.googleResponse}
          onFailure={this.googleResponse}
        />
      </Container>
    );
  }
  private googleResponse = (response: any) => {
    const axiosInstance = axios.create({
      validateStatus: status => {
        return status === 200;
      }
    });
    axiosInstance
      .post("/api/v1/accounts/auth/google", {
        access_token: response.accessToken
      })
      .then(apiResponse => {
        if (apiResponse) {
          // tslint:disable-next-line: no-console
          console.log(apiResponse.data); // this is the user object
        }
      });
  };
  private handleChange(event: React.FormEvent<EventTarget>) {
    const target = event.target as HTMLInputElement;

    const newState = Object.assign({}, this.state);
    newState[target.name] = target.value;

    this.setState(newState);
  }
  private handleTermsChange(checked: boolean | undefined) {
    this.setState(current => ({ ...current, agreeToTerms: checked || false }));
  }

  private handleSubmit(event: React.FormEvent<EventTarget>) {
    event.preventDefault();

    const errors = this.signupformValidator.validate(this.state);
    if (errors.form) {
      this.setState(current => ({ ...current, success: false, errors }));
      return;
    }
    // post
    const axiosInstance = axios.create({
      validateStatus: status => {
        return status === 200;
      }
    });
    axiosInstance
      .post("/api/v1/accounts/signup", {
        username: this.state.email,
        ...this.state
      })
      .then(response => {
        // clear form if successful
        this.setState({
          agreeToTerms: false,
          confirmPassword: "",
          email: "",
          errors: new SignupFormErrors(),
          firstname: "",
          lastname: "",
          password: "",
          success: true
        });
      })
      .catch(error => {
        const reqErrors = new SignupFormErrors();
        reqErrors.form = true;
        reqErrors.messages.push(error.response.data.message);
        this.setState(current => ({
          ...current,
          errors: reqErrors,
          success: false
        }));
      });
  }
}

export default SignupForm;
