import axios from "axios";
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import 'semantic-ui-css/semantic.min.css';
import App from './App';
import './index.css';
// import registerServiceWorker from './registerServiceWorker';

axios.defaults.xsrfHeaderName = "X-CSRF-Token";
axios.defaults.xsrfCookieName = "STARTER_XSRF_TOKEN";

ReactDOM.render(
  <BrowserRouter>
    <App /></BrowserRouter>,
  document.getElementById('root') as HTMLElement
);
// registerServiceWorker();
