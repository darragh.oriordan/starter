import * as React from 'react';
import PulsemapMeta from '../components/Meta/Pulsemap'
import PulsemapMap from '../components/Pulsemap'

class Pulsemap extends React.Component<{}, {}> {

    constructor(props: {}) {
        super(props)
    }

    public render() {
        return (
            <div>
                <PulsemapMeta />
                <PulsemapMap />
            </div>
        );
    }
}

export default Pulsemap;