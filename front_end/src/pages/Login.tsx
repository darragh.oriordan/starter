import * as React from 'react';
import { Link } from 'react-router-dom';
import { Header } from 'semantic-ui-react';
import LoginForm from '../components/LoginForm'
import MainMenu from '../components/MainMenu'
import LoginMeta from '../components/Meta/Login'

class Login extends React.Component<{}, {}> {

    constructor(props: {}) {
        super(props)
    }

    public render() {
        return (
            <div>
                <LoginMeta />
                <MainMenu />
                <div style={{ marginTop: '7em' }}>
                    <Header as='h1'>Log in to get started</Header>
                    <p>Inspire your team and inform your customers. Do it all with [[sites]]’s visualizations. Already have an account? <Link to="/login">Log in</Link></p>
                    <LoginForm />                  
                </div>
            </div>
        );
    }
}

export default Login;