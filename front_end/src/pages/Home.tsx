import * as React from 'react';

import { Container } from 'semantic-ui-react';
import MainMenu from '../components/MainMenu';
import HomeMeta from '../components/Meta/Homepage'
// import './Home.css';

class Home extends React.Component<{}, {}> {

    constructor(props: {}) {
        super(props)

    }

    public render() {
        return (
            <Container>
                <HomeMeta />
                <MainMenu />
            </Container>
        );
    }
}

export default Home;