import * as React from "react";
import { Header } from "semantic-ui-react";
import LogoutForm from "src/components/LogoutForm";
import MainMenu from "../components/MainMenu";
import LoginMeta from "../components/Meta/Login";

class Logout extends React.Component<{}, {}> {
  constructor(props: {}) {
    super(props);
  }

  public render() {
    return (
      <div>
        <LoginMeta />
        <MainMenu />
        <div style={{ marginTop: "7em" }}>
          <Header as="h1">Logout</Header>

          <LogoutForm />
        </div>
      </div>
    );
  }
}

export default Logout;
