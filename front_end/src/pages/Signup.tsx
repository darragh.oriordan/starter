import * as React from 'react';
import { Link } from 'react-router-dom';
import { Header } from 'semantic-ui-react';
import MainMenu from '../components/MainMenu'
import SignupMeta from '../components/Meta/Signup'
import SignupForm from '../components/SignupForm'

class Signup extends React.Component<{}, {}> {

    constructor(props: {}) {
        super(props)
    }

    public render() {
        return (
            <div>
                <SignupMeta />
                <MainMenu />
                <div style={{ marginTop: '7em' }}>
                    <Header as='h1'>Get started with a free account</Header>
                    <p>Inspire your team and inform your customers. Do it all with [[sites]]’s visualizations. Already have an account? <Link to="/login">Log in</Link></p>
                    <SignupForm />
                </div>
            </div>
        );
    }
}

export default Signup;