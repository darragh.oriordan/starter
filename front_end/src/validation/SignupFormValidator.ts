import { ISignupFormState } from "src/components/SignupForm";
import SignupFormErrors from "src/models/SignupFormErrors";

class SignupFormValidator {
    public validate(state: ISignupFormState): SignupFormErrors {
        const errors = new SignupFormErrors();

        if (!this.validateStringProvided(state.email)) {
            errors.email = true
            errors.messages.push("Email must be provided")
        }
        
        if (!this.validateStringProvided(state.firstname)) {
            errors.firstname = true
            errors.messages.push("First name must be provided")
        }
        
        if (!this.validateStringProvided(state.lastname)) {
            errors.lastname = true
            errors.messages.push("Last name must be provided")
        }
        
        if (!this.validateStringProvided(state.password)) {
            errors.password = true
            errors.messages.push("Password must be provided")
        }
        
        if (!this.validateStringProvided(state.confirmPassword)) {
            errors.confirmPassword = true
            errors.messages.push("Password confirmation must be provided")
        }
        
        if (!this.validatePasswordMatch(state.password, state.confirmPassword)) {
            errors.password = true
            errors.confirmPassword = true
            errors.messages.push("Password and confirmation must match")
        }
        if (!this.validateTermsAccepted(state.agreeToTerms)) {
            errors.agreeToTerms = true
            errors.messages.push("You must agree to the terms")
        }
        if (!this.validateEmailFormat(state.email)) {
            errors.email = true
            errors.messages.push("Email should have an @")
        }
        if (!this.validateEmailLength(state.email)) {
            errors.email = true
            errors.messages.push("Email should be longer than 3 characters")
        }

        errors.form = errors.agreeToTerms
            || errors.confirmPassword
            || errors.email
            || errors.firstname
            || errors.lastname
            || errors.password;

        return errors;
    }

    private validateStringProvided(input: string): boolean {
        return input !== ""
    }

    private validateEmailFormat(email: string): boolean {
        return email.indexOf("@") > 0
    }
    private validateEmailLength(email: string): boolean {
        return email.length > 3
    }

    private validatePasswordMatch(p1: string, p2:string): boolean {
        return p1 === p2
    }
    private validateTermsAccepted(input:boolean): boolean {
        return input
    }
}

export default SignupFormValidator;