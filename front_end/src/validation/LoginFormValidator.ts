import { ILoginFormState } from "src/components/LoginForm";
import LoginFormErrors from "src/models/LoginFormErrors";

class LoginFormValidator {
    public validate(state: ILoginFormState): LoginFormErrors {
        const errors = new LoginFormErrors();

        if (!this.validateStringProvided(state.email)) {
            errors.email = true
            errors.messages.push("Email must be provided")
        }

        if (!this.validateStringProvided(state.password)) {
            errors.password = true
            errors.messages.push("Password must be provided")
        }

        if (!this.validateEmailFormat(state.email)) {
            errors.email = true
            errors.messages.push("Email should have an @")
        }
        if (!this.validateEmailLength(state.email)) {
            errors.email = true
            errors.messages.push("Email should be longer than 3 characters")
        }

        errors.form =
            errors.email
            || errors.password;

        return errors;
    }

    private validateStringProvided(input: string): boolean {
        return input !== ""
    }

    private validateEmailFormat(email: string): boolean {
        return email.indexOf("@") > 0
    }
    private validateEmailLength(email: string): boolean {
        return email.length > 3
    }
}

export default LoginFormValidator;