# Starter for js projects

I wanted a starter with

-   SQL and orm that I understand and can run on a free host
-   Separate projects so the front end can be anything as long as it's hostable
-   But an ok dev environment
-   Auth setup already - i use cookies and sessions for an api
-   Sessions and everything stored on database so the app can scale somewhat
-   Multi processor support for express
-   Basic security taken care of
-   Basic CI working on some platform (gitlab in this case)
-   Basic CD to staging working on some platform (gitlab in this case)
-   https on localhost

## Setup before run:

-   have node 10.14.2 LTS installed - (https://nodejs.org/en/)
-   have docker installed - https://www.docker.com/get-started
-   clone the project in a directory
-   Run `cd <project root>` to be in the root directory for the project
-   Run `yarn add -g nodemon`
-   Run `yarn db-build`
-   Run `yarn install`
-   Run `cd back_end`
-   Create a new file called ".env" if not existed in backend folder, use .env.example by copying and pasting as .env
-   Run `npm run db:migrate` to setup/modify tables in database
-   Run `npm run db-seed` to insert data into database
-   Run `cd ..` (to get to the root folder again)
-   Run `yarn install`

## To run the app:

First, be in the root of project, then:

### To run server and front end together:

-   Run `yarn dev` (this will start the server and the front end)

### To run back end only:

-   Run `yarn start-server`

### To run front end only:

-   Run `yarn start-client`

### To set up ssl on your dev machine

-   You must have openssl - you're fine on mac. On windows use GitBash or similar.
-   the front end on https://localhost:3000 will work as normal. The back_end only serves the BUILT front end. So you must build the front end before you will see any changes if you go to https://localhost:5443 (the server https url)
-   Run `cd back_end`
-   Run `yarn setup-localhost-ssl`
-   Now export the cert from chrome and add to your trusted certificate store. Follow the instructions in https://stackoverflow.com/questions/21397809/create-a-trusted-self-signed-ssl-cert-for-localhost-for-use-with-express-node to add the cert to your trusted store.

### To setup a google auth key

-   Visit Google Cloud Console
-   Click on the Create Project button
-   Enter Project Name, then click on Create button
-   Then click on APIs & auth in the sidebar and select API tab
-   Click on Google+ API under Social APIs, then click Enable API
-   Next, under APIs & auth in the sidebar click on Credentials tab
-   Click on Create new Client ID button
-   Select Web Application and click on Configure Consent Screen
-   Fill out the required fields then click on Save
-   In the Create Client ID modal dialog:
-   Application Type: Web Application
-   Authorized Javascript origins: http://localhost:5000 (and second entry for port 3000)
-   Authorized redirect URI: http://localhost:5000/accounts/auth/google/callback (and second entry for port 3000)
-   Click on Create Client ID button
-   Copy and paste Client ID and Client secret keys into .env
